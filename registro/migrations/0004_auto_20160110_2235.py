# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0003_sucursal'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='empresa',
            name='latitud',
        ),
        migrations.RemoveField(
            model_name='empresa',
            name='longitud',
        ),
        migrations.AddField(
            model_name='sucursal',
            name='latitud',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sucursal',
            name='longitud',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
