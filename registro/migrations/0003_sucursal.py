# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0002_auto_20151202_1708'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sucursal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=700, null=True, blank=True)),
                ('direccion', models.CharField(max_length=700, null=True, blank=True)),
                ('telefono', models.IntegerField(null=True, blank=True)),
                ('empresa', models.ForeignKey(blank=True, to='registro.Empresa', null=True)),
            ],
            options={
                'verbose_name_plural': 'Sucursales',
            },
        ),
    ]
