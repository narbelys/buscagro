# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='productor',
            options={'verbose_name_plural': 'Productores'},
        ),
        migrations.AddField(
            model_name='empresa',
            name='email_vendedor',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='empresa',
            name='es_vendedor',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='empresa',
            name='foto_empresa',
            field=models.FileField(null=True, upload_to=b'fotos_empresas', blank=True),
        ),
        migrations.AddField(
            model_name='producto',
            name='foto_producto',
            field=models.FileField(null=True, upload_to=b'fotos_productos', blank=True),
        ),
    ]
