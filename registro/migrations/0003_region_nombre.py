# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0002_auto_20150913_0430'),
    ]

    operations = [
        migrations.AddField(
            model_name='region',
            name='nombre',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
