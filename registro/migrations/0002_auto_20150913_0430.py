# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='region',
            name='nombre',
        ),
        migrations.AddField(
            model_name='productor',
            name='empresa',
            field=models.ForeignKey(blank=True, to='registro.Empresa', null=True),
        ),
        migrations.AlterField(
            model_name='region',
            name='numero',
            field=models.IntegerField(choices=[(1, 'Tarapac\xe1'), (2, 'Antofagasta'), (3, 'Atacama'), (4, 'Coquimbo'), (5, 'Valpara\xedso'), (6, "O'Higgins"), (7, 'Maule'), (8, 'BioB\xedo'), (9, 'La Araucan\xeda'), (10, 'Los Lagos'), (11, 'Aysen'), (12, 'Magallanes'), (13, 'Metropolitana'), (14, 'Los R\xedos'), (15, 'Arica y Parinacota')]),
        ),
    ]
