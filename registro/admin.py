from django.contrib import admin
from registro.models import *
import datetime
from django.http import HttpResponse
import csv
from django.utils import timezone
# Register your models here.


def export_as_csv_action(description="Export selected objects as CSV file",
                         fields=None, exclude=None, header=True):

    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """
    def export_as_csv(modeladmin, request, queryset):
        """
        Generic csv export admin action.
        based on http://djangosnippets.org/snippets/1697/
        """
        opts = modeladmin.model._meta
        field_names = set([field.name for field in opts.fields])
        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset
        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset

        today = datetime.datetime.now()
        today = "__%s/%s/%s" % (today.day, today.month, today.year) + \
                "__%sh%sm%ss" % (today.hour, today.month, today.second)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s' % \
                    unicode(opts).replace('.', '_') + today + '.csv'
        writer = csv.writer(response)
        if header:
            array=list(field_names)
            array = array + ['id_productor', 'tipo_especie', 'especies' , 'productor', 'tipo_especie',  'empresa', 'rut_empresa', 'telefono_empresa', 'direccion_empresa', 'region_empresa', 'provincia_empresa', 'comuna_empresa', 'email_vendedor'] 
            writer.writerow(array)

        for obj in queryset:
            row = []
            for field in field_names:
                #if 'datetime.datetime' in str(type(getattr(obj, field))):
                #    row = row + [getattr(obj, field)
                #                    .astimezone(timezone('America/Santiago'))]
                #else:
                row = row + [unicode(getattr(obj, field)).encode('utf-8')]
            #ProductorEspecie
            id_productor = obj.productor.pk
            tipo_especie = obj.tipo_especie.nombre
            especies=''
            for i in obj.especie.all():
                especies = especies + '-' + i.nombre
                
            productor = obj.productor.username
            tipo_especie = obj.tipo_especie.nombre  
            empresa = obj.productor.empresa.razon_social   
            rut_empresa = obj.productor.empresa.rut   
            telefono_empresa = obj.productor.empresa.telefono   
            direccion_empresa = obj.productor.empresa.direccion   
            region_empresa = obj.productor.empresa.region.nombre   
            provincia_empresa = obj.productor.empresa.provincia.nombre   
            comuna_empresa = obj.productor.empresa.comuna.nombre   
            email_vendedor = obj.productor.empresa.email_vendedor
    
            row = row+ [id_productor, tipo_especie, especies, productor, tipo_especie, empresa, rut_empresa, telefono_empresa, direccion_empresa, region_empresa, provincia_empresa, comuna_empresa, email_vendedor]
            writer.writerow(row)
        return response
    export_as_csv.short_description = description
    return export_as_csv

class ProductorEspecieAdmin(admin.ModelAdmin):
    list_display = ['name_productor', 'tipo_especie', 'hectarea']
    actions = [export_as_csv_action("Exportar a csv",
               fields=['hectarea'],
               header=True)]
    def name_productor(self, obj):
        return obj.productor.username    
    def tipo_especie(self, obj):
        return obj.tipo_especie.nombre       
    def empresa(self, obj):
        return obj.productor.empresa.razon_social   
    
admin.site.register(Region)
admin.site.register(Provincia)
admin.site.register(Comuna)
admin.site.register(Productor)
admin.site.register(Empresa)
admin.site.register(ProductorEspecie, ProductorEspecieAdmin)
#admin.site.register(ProductorEspecie)
admin.site.register(Usuario)
admin.site.register(TipoEspecie)
admin.site.register(Especie)
admin.site.register(Producto)
admin.site.register(TipoProductos)
admin.site.register(Sucursal)
admin.site.register(Tips)
