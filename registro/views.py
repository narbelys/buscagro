# -*- encoding: utf-8 -*-
import os
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from registro.models import *
from registro.utils import creaCorreoBienvenida
from home.views import chequea_usuario

# Create your views here.
@csrf_exempt
def crearegistro(request):
	# Empresa
	razon_social = request.POST.get('razon-social')
	rut = request.POST.get('rut-empresa').replace('.','').replace('-','')
	telefono = request.POST.get('telefono')
	direccion = request.POST.get('direccion')
	region = request.POST.get('region-empresa')
	provincia = request.POST.get('provincia-empresa')
	comuna = request.POST.get('comuna-empresa')
	# Productor
	username = request.POST.get('usuario')
	password = request.POST.get('password')
	email = request.POST.get('email')
	#Especies
	frutales = request.POST.getlist('option-frutales')
	h_frutales = request.POST.get('h-frutales')
	semillas = request.POST.getlist('option-semillas')
	h_semillas = request.POST.get('h-semillas')

	# Se crea empresa
	region = Region.objects.get(numero=int(region))
	provincia = Provincia.objects.get(id=int(provincia), region=region)
	comuna = Comuna.objects.get(id=int(comuna), provincia=provincia)
	print "creando empresa"
	empresa = Empresa.objects.create(
		razon_social = razon_social,
		rut = rut,
		telefono = telefono,
		direccion = direccion,
		region = region,
		provincia = provincia,
		comuna = comuna,
		)

	productor = Productor.objects.create(
		username = username,
		password = password,
		email = email,
		empresa = empresa
		)

	tipo_especie_frutales = TipoEspecie.objects.get(nombre='Frutales')
	productor_especie_frutales = None
	for f in frutales:
		especie = Especie.objects.get( nombre = f )
		productor_especie_frutales = ProductorEspecie.objects.create(
			productor = productor,
			tipo_especie = tipo_especie_frutales,
			hectarea = h_frutales
			)
		productor_especie_frutales.especie.add(especie)
	if productor_especie_frutales is not None:
		productor_especie_frutales.save()

	tipo_especie_semillas = TipoEspecie.objects.get(nombre='Semillas')
	productor_especie_semillas = None
	for s in semillas:
		especie = Especie.objects.get( nombre = s )
		productor_especie_semillas = ProductorEspecie.objects.create(
			productor = productor,
			tipo_especie = tipo_especie_semillas,
			hectarea = h_semillas
			)
		productor_especie_semillas.especie.add(especie)
	if productor_especie_semillas is not None:
		productor_especie_semillas.save()

	#Se envia correo de bienvenida
	path_html = creaCorreoBienvenida(username)
	if path_html != '':
		print path_html
		os.system("/usr/bin/mutt -e \"set content_type=text/html\" -e \"set from='Buscagro. <contacto@buscagro.cl>'\" -b grungero@gmail.com '" + email + "' -s 'Bienvenido a Buscagro' < "+path_html)
	else:
		print "error"
		return redirect('home')


	return redirect('home')

def carga_datos_modificar(request):
	usuario = chequea_usuario(request)
	productor = Productor.objects.get(username=usuario)
	empresa = productor.empresa
	productorespecie = ProductorEspecie.objects.filter(productor=productor)
	data = {
		'productor':productor,
		'empresa':empresa,
		'productorespecie':productorespecie
	}
	return render(request, 'modal_modifica_registro.html', data)

@csrf_exempt
def modificaregistro(request):
	exito = 1
	try:
		id_productor = int(request.POST.get('id-productor'))
		username = request.POST.get('usuario')
		password = request.POST.get('password')
		email = request.POST.get('email')
		productor = Productor.objects.filter(id=id_productor).update(
			username=username,
			password=password,
			email=email
			)
		request.session['productor_id'] = username
	except Exception as e:
		print e
		exito = 0
	return redirect('home')



@csrf_exempt
def verifica_existencia_rut(request):
	rut = request.POST.get('rut')#.replace('.','').replace('-','')
	print "ruuut",rut
	try:
		Empresa.objects.get(rut=rut)
		return HttpResponse(0)
	except Exception, e:
		return HttpResponse(1)

@csrf_exempt
def loadprovincias(request):
	region = request.POST.get('region')
	provincias = Provincia.objects.filter(region__numero=int(region))
	data = {
		'provincias':provincias
	}
	return render(request, 'listado_provincias.html', data)

@csrf_exempt
def loadcomunas(request):
	provincia = request.POST.get('provincia')
	comunas = Comuna.objects.filter(provincia__id=int(provincia))
	data = {
		'comunas':comunas
	}
	return render(request, 'listado_comunas.html', data)
