# -*- encoding: utf-8 -*-
from django.db import models
import os

REGIONES = (
    (1, u"Tarapacá"),
    (2, u"Antofagasta"),
    (3, u"Atacama"),
    (4, u"Coquimbo"),
    (5, u"Valparaíso"),
    (6, u"O'Higgins"),
    (7, u"Maule"),
    (8, u"BioBío"),
    (9, u"La Araucanía"),
    (10, u"Los Lagos"),
    (11, u"Aysen"),
    (12, u"Magallanes"),
    (13, u"Metropolitana"),
    (14, u'Los Ríos'),
    (15, u'Arica y Parinacota'),
)

envoltorios = (
                ('-','-'),
                ('0,25 Lt','0,25 Lt'),
                ('1 Lt','1 Lt'),
                ('3,8 Lt','3,8 Lt'),
                ('5 Lt','5 Lt'),
                ('10 Lt','10 Lt'),
                ('20 Lt','20 Lt'),
                ('25 Lt','25 Lt'),
                ('205 Lt','205 Lt'),
                ('0,5 Kg','0,5 Kg'),
                ('1 Kg','1 Kg'),
                ('5 Kg','5 Kg'),
                ('10 Kg','10 Kg'),
                ('18 Kg','18 Kg'),
                ('20 Kg','20 Kg'),
                ('25 Kg','25 Kg'),
                ('50 Kg','50 Kg'),
               )

# Create your models here.
class Usuario(models.Model):
    email = models.CharField(max_length=200)

    def __unicode__(self):
        return self.email

class Region(models.Model):
    numero = models.IntegerField(choices=REGIONES)
    nombre = models.CharField(max_length=200, blank=True, null=True)

    def __unicode__(self):
        return str(self.nombre)

class Provincia(models.Model):
    region = models.ForeignKey(Region)
    nombre = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nombre + " - Region de " + self.region.nombre

class Comuna(models.Model):
    provincia = models.ForeignKey(Provincia)
    nombre = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nombre + " - Provincia de " + self.provincia.nombre

class Empresa(models.Model):
    razon_social = models.CharField(max_length=200)
    rut = models.CharField(max_length=54)
    telefono = models.IntegerField()
    direccion = models.CharField(max_length=200)
    region = models.ForeignKey(Region)
    provincia = models.ForeignKey(Provincia)
    comuna = models.ForeignKey(Comuna)
    foto_empresa = models.FileField(upload_to='/home/augustoa/webapps/buscagrostatic/media/fotos_empresas/', null=True, blank=True)
    es_vendedor = models.BooleanField(default=False)
    email_vendedor = models.CharField(max_length=200, blank=True, null=True)

    def __unicode__(self):
        return self.razon_social

    def filename(self):
        return os.path.basename(self.foto_empresa.name)


class Sucursal(models.Model):
    nombre = models.CharField(max_length=700, blank=True, null=True)
    direccion = models.CharField(max_length=700, blank=True, null=True)
    telefono = models.IntegerField(blank=True, null=True)
    latitud = models.CharField(max_length=255, null=True, blank=True)
    longitud = models.CharField(max_length=255, null=True, blank=True)
    empresa = models.ForeignKey(Empresa, blank=True, null=True)

    def __unicode__(self):
        return self.empresa.razon_social + " - " + self.nombre

    class Meta:
        verbose_name_plural = "Sucursales"
        
class Tips(models.Model):
    nombre = models.CharField(max_length=700, blank=True, null=True)
    direccion = models.CharField(max_length=700, blank=True, null=True, verbose_name="Descripcion")
    #telefono = models.IntegerField(blank=True, null=True)
    latitud = models.CharField(max_length=255, null=True, blank=True)
    longitud = models.CharField(max_length=255, null=True, blank=True)
    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Tips"

class Productor(models.Model):
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    empresa = models.ForeignKey(Empresa, null=True, blank=True)

    def __unicode__(self):
        return self.username

    class Meta:
        verbose_name_plural = "Productores"

class TipoEspecie(models.Model):
    codigo = models.IntegerField()
    nombre = models.CharField(max_length=200)

    def __unicode__(self):
        return str(self.codigo) + " - " + self.nombre

class Especie(models.Model):
    nombre = models.CharField(max_length=200)
    tipo_especie = models.ForeignKey(TipoEspecie)

    def __unicode__(self):
        return self.nombre

class ProductorEspecie(models.Model):
    productor = models.ForeignKey(Productor)
    especie = models.ManyToManyField(Especie, null=True, blank=True)
    tipo_especie = models.ForeignKey(TipoEspecie, null=True, blank=True)
    hectarea = models.IntegerField()

    def __unicode__(self):
        return self.productor.username + " - Tipo especie: "+ str(self.tipo_especie.nombre)

#1:Agroquimico
#2:Fertilizante
#3:Otros Productos
class TipoProductos(models.Model):
    numero = models.IntegerField()
    nombre = models.CharField(max_length=200)
    
    def __unicode__(self):
        return str(self.numero) + " - " +self.nombre
    
    class Meta:
        verbose_name_plural = "Tipo Productos"

class Producto(models.Model):
    nombre = models.CharField(max_length=250, blank=True, null=True)
    tipo_producto = models.ForeignKey(TipoProductos)
    archivo_pdf = models.FileField(upload_to='pdf', blank=True, null=True)
    foto_producto = models.FileField(upload_to='fotos_productos', blank=True, null=True)

    def __unicode__(self):
        return self.nombre
