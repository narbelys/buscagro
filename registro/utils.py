# -*- encoding: utf-8 -*-
import os
from datetime import datetime
from django.template.loader import render_to_string
import cStringIO as StringIO
import json
from registro.models import *

CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
TEMPLATE_DIR = os.path.join(CURRENT_DIR, '..','templates')
PATH_CORREOS = os.path.join(CURRENT_DIR,'..','templates','correos')
PATH_CORREOS_ENVIADOS = os.path.join(CURRENT_DIR,'..','templates','correos_enviados')

def creaCorreoBienvenida(usuario):
    now = datetime.now()
    fecha_junta = str(now.year)+str(now.month)+str(now.day)+'_'+str(now.hour)+str(now.minute)+str(now.second)
    nombre_archivo_bienvenida_usuario = PATH_CORREOS_ENVIADOS+'/bienvenida/bienvenida_'+fecha_junta+'.html'
    response = render_to_string(PATH_CORREOS+'/mail-bienvenida-buscagro.html', { 'usuario': usuario })

    try:                
        with open(nombre_archivo_bienvenida_usuario, 'w') as static_file:
            static_file.write(response)
    except Exception, e:
        print e
        return ''  
    return nombre_archivo_bienvenida_usuario