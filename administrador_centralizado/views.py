from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from registro.models import *
from oferta.models import *
from forms import *
import xlrd

# Create your views here.

def home(request):
    data = {'user':'hola'}
    return render(request, 'administrador_centralizado/home.html',data)

@csrf_exempt
def cargar_productos(request):
    if request.method == 'POST':
        form = CargaForm(request.POST, request.FILES)
        if form.is_valid():
            archivo = form.cleaned_data['archivo']
            workbook = xlrd.open_workbook(file_contents=archivo.read())
            for pag in workbook.sheet_names():
                pagina = workbook.sheet_by_name(pag)
                numero_filas = pagina.nrows
                for row_idx in range(1, numero_filas):
                    numero = pagina.cell(row_idx, 0)
                    nombre = pagina.cell(row_idx, 1)
                    producto = pagina.cell(row_idx, 2)
                    obj = dict()
                    tipo_producto_numero = numero.value
                    tipo_producto_nombre = nombre.value
                    nombre_producto = producto.value

                    print tipo_producto_numero
                    print tipo_producto_nombre
                    print nombre_producto
                    try:
                        tipo_e = TipoProductos.objects.get(numero=int(tipo_producto_numero), nombre = tipo_producto_nombre)
                        print "1"
                    except TipoProductos.DoesNotExist:
                        tipo_e = TipoProductos.objects.create(numero=int(tipo_producto_numero), nombre = tipo_producto_nombre)
                        print "2"
                    except Exception as e:
                        print e
                        continue
                    try:
                        producto = Producto.objects.get(nombre=nombre_producto, tipo_producto = tipo_e)
                    except Producto.DoesNotExist:
                        producto = Producto.objects.create(nombre=nombre_producto, tipo_producto = tipo_e)
        else:
            print "form no valido"
    else:   
        form = CargaForm()
    data = {'form': form, 
            'titulo_pagina': 'Carga Productos'}
    return render(request, 'administrador_centralizado/cargas_producto/index.html', data)


def cellval(cell, datemode):
    if cell.ctype == xlrd.XL_CELL_DATE:
        datetuple = xlrd.xldate_as_tuple(cell.value, datemode)
        if datetuple[3:] == (0, 0, 0):
            return datetime.date(datetuple[0], datetuple[1], datetuple[2])
        return datetime.date(datetuple[0], datetuple[1], datetuple[2], datetuple[3], datetuple[4], datetuple[5])
    if cell.ctype == xlrd.XL_CELL_EMPTY:    return ''
    if cell.ctype == xlrd.XL_CELL_BOOLEAN:  return cell.value == 1
    if cell.ctype == xlrd.XL_CELL_NUMBER and int(cell.value) == cell.value:
        return int(cell.value)
    return cell.value

def historial_cotizaciones(request):
    cotizaciones = CotizacionProductorOferta.objects.all()
    data = {
        'cotizaciones':cotizaciones,
        'titulo_pagina':'Historial de Cotizaciones'
    }
    return render(request, 'administrador_centralizado/historial_cotizaciones/index.html', data)