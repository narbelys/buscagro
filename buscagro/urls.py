"""buscagro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from registro import views as registro_views
from home import views as home_views
from oferta import views as oferta_views
from administrador_centralizado import views as administrador_centralizado_views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', home_views.home, name='home'),
    url(r'^login/$', home_views.login, name='login'),
    url(r'^logout/$', home_views.logout, name='logout'),
    url(r'^recuperapassword/$', home_views.enviar_clave, name='recuperapassword'),
    url(r'^sendmail/$', home_views.sendmail, name='sendmail'),
    url(r'^contacto/$', home_views.contacto, name='contacto'),
    url(r'^guardar_email/', home_views.guardar_email, name='guardar_email'),
    url(r'^buscar/', oferta_views.buscar, name='buscar'),
    url(r'^buscar_por_categoria/', oferta_views.buscar_por_categoria, name='buscar_por_categoria'),
    url(r'^lista_productos/', oferta_views.listar_productos, name='listar_productos'),
    url(r'^lista_regiones/', oferta_views.listar_regiones, name='listar_regiones'),
    url(r'^ofertas_vermas', oferta_views.vermas, name='oferta_vermas'),
    url(r'^envia_cotizacion/', oferta_views.envia_cotizacion, name='envia_cotizacion'),
    url(r'^quienes_somos/', home_views.quienes_somos, name='quienes_somos'),
    url(r'^preguntas_frecuentes/', home_views.preguntas_frecuentes, name='preguntas_frecuentes'),
    url(r'^politicas/', home_views.politicas, name='politicas'),
    url(r'^terminos_condiciones/', home_views.terminos_condiciones, name='terminos_condiciones'),
    url(r'^crearegistro/', registro_views.crearegistro, name='crearegistro'),
    url(r'^modificaregistro/', registro_views.modificaregistro, name='modificaregistro'),
    url(r'^loadprovincias/', registro_views.loadprovincias, name='loadprovincias'),
    url(r'^loadcomunas/', registro_views.loadcomunas, name='loadcomunas'),
    url(r'^carga_datos_modificar/', registro_views.carga_datos_modificar, name='carga_datos_modificar'),
    url(r'^verifica_existencia_rut/', registro_views.verifica_existencia_rut, name='verifica_existencia_rut'),
    url(r'^administrador_centralizado/home', administrador_centralizado_views.home, name='administrador_centralizado_home'),
    url(r'^administrador_centralizado/cargar_productos/', administrador_centralizado_views.cargar_productos, name='administrador_centralizado_cargar_productos'),
    url(r'^administrador_centralizado/historial_cotizaciones/', administrador_centralizado_views.historial_cotizaciones, name='administrador_centralizado_historial_cotizaciones'),

] 

#+ static(settings.STATIC_URL_BASE, document_root=settings.STATIC_PATH, show_indexes=True)
