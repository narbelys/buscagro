'''
Created on 06-04-2014

@author: andres
'''

import os
import sys

CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))

sys.path.insert(0, CURRENT_DIR)
sys.path.insert(1, os.path.join(CURRENT_DIR, '..'))

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
