# Create your views here.
import os
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from registro.models import *
from oferta.models import *
from home.views import chequea_usuario
import json
from datetime import datetime
from django.template.loader import render_to_string

CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
TEMPLATE_DIR = os.path.join(CURRENT_DIR, '..','templates')
PATH_CORREOS = os.path.join(TEMPLATE_DIR,'correos')
PATH_CORREOS_ENVIADOS = os.path.join(TEMPLATE_DIR,'correos_enviados')

@csrf_exempt
def buscar(request, categoria=None):
    usuario = chequea_usuario(request)
    ofertas = []
    dict_ofertas = {}
    dict_categorias = {}
    numero_resultados = None
    empresas = Empresa.objects.all()
    ofertas_mapa = []
    tips = Tips.objects.all()
    ofertas = Oferta.objects.all()
    es_post = []
    producto_seleccionado = ''
    region_provincia_seleccionada = ''
    if request.POST:
        lugar = []
        producto = request.POST.get('producto','')
        ciudad = request.POST.get('ciudad','')
        id_oferta_mapa = request.POST.get('id-oferta-mapa','')
        #Si es que se hizo click en el mapa o en el boton buscar
        if id_oferta_mapa != '':
            ofertas = Oferta.objects.filter(id=int(id_oferta_mapa))
        else:
            if ciudad != '':
                region_provincia_seleccionada = ciudad
                try:
                    region = Region.objects.get(nombre__icontains=ciudad)
                    ofertas = Oferta.objects.filter(sucursal__empresa__region=region)
                except Region.DoesNotExist:
                    region = Provincia.objects.get(nombre__icontains=ciudad)
                    ofertas = Oferta.objects.filter(sucursal__empresa__provincia=region)
            else:
                ofertas = Oferta.objects.all()
        contador = 0
        #Se recorren las ofertas para listarlas
        print producto
        for o in ofertas:
            if producto != '':
                producto_seleccionado = producto
                product = Producto.objects.filter(nombre__icontains=producto) 
                if not product:
                    tipoproductos = TipoProductos.objects.filter(nombre__icontains=producto) 
                    product = Producto.objects.filter(tipo_producto__in = tipoproductos) 
                    producto_oferta = ProductoEnOferta.objects.filter(oferta=o, producto__in=product)
                else:
                    producto_oferta = ProductoEnOferta.objects.filter(oferta=o, producto__in=product)
            else:
                producto_oferta = ProductoEnOferta.objects.filter(oferta=o)
            if producto_oferta:
                dict_ofertas[o] = producto_oferta
                contador = contador + 1
                numero_resultados = contador
        ofertas_vistas = []
       
        
        for key,value in dict_ofertas.items():
            if key not in ofertas_vistas:
                try:
                    #foto_empresa_path = key.sucursal.empresa.foto_empresa.url
                    foto_empresa_path = '/static/media/fotos_empresas/' + key.sucursal.empresa.filename()
		    #foto_empresa_path = foto_empresa_path.split('/')
		    #foto_empresa_path = foto_empresa_path[len(foto_empresa_path-1)]
                except:
                    foto_empresa_path = ''
                ofertas_tmp = {'direccion': key.sucursal.direccion,'razon_social': key.sucursal.nombre,'latitud': key.sucursal.latitud,'longitud': key.sucursal.longitud,'id_oferta': key.id,'foto_empresa':foto_empresa_path}
                ofertas_mapa.append(ofertas_tmp)
                ofertas_vistas.append(key)
        print '1------->', tips
        for i in tips:
            tips_tmp = {'direccion': i.direccion,'razon_social': i.nombre,'latitud': i.latitud,'longitud': i.longitud,'id_oferta': i.id,'foto_empresa':'', 'type': 'tips'}
            ofertas_mapa.append(tips_tmp)
            
            
        es_post = [{'es_post':1}]
    else:
        ofertas_vistas = []
        es_post = [{'es_post':0}]
        for o in ofertas:
            producto_oferta = ProductoEnOferta.objects.filter(oferta=o)
            dict_ofertas[o] = producto_oferta
            for pp in producto_oferta:
                dict_categorias[pp.producto.tipo_producto.numero] = pp.producto.tipo_producto.nombre
            if o not in ofertas_vistas:
                try:
                    #foto_empresa_path = o.sucursal.empresa.foto_empresa.url
                    foto_empresa_path = '/static/media/fotos_empresas/' + o.sucursal.empresa.filename()
                    #foto_empresa_path = foto_empresa_path.split('/')
                    #foto_empresa_path = foto_empresa_path[len(foto_empresa_path-1)]
                except:
                    foto_empresa_path = ''
                ofertas_tmp = {'direccion': o.sucursal.direccion,'razon_social': o.sucursal.nombre,'latitud': o.sucursal.latitud,'longitud': o.sucursal.longitud,'id_oferta': o.id,'foto_empresa':foto_empresa_path}
                ofertas_mapa.append(ofertas_tmp)
                ofertas_vistas.append(o)
        for i in tips:
            tips_tmp = {'direccion': i.direccion,'razon_social': i.nombre,'latitud': i.latitud,'longitud': i.longitud,'id_oferta': i.id,'foto_empresa':'' , 'type': 'tips' }
            ofertas_mapa.append(tips_tmp)
    tiempos = [{'year_fin': o.fecha_fin.year,'month_fin': o.fecha_fin.month,'day_fin': o.fecha_fin.day, 'hour_fin': o.fecha_fin.hour,'minute_fin': o.fecha_fin.minute, 'id_oferta': o.id} for o in ofertas]

    data = {
        'ofertas':dict_ofertas,
        'categorias':dict_categorias,
        'usuario':usuario,
        'numero_resultados':numero_resultados,
        'tiempos':json.dumps(tiempos),
        'ofertas_mapa':json.dumps(ofertas_mapa),
        'es_post':json.dumps(es_post),
        'region_provincia_seleccionada':region_provincia_seleccionada,
        'producto_seleccionado': producto_seleccionado
    }
    return render(request, 'interior.html', data)

@csrf_exempt
def buscar_por_categoria(request):
    usuario = chequea_usuario(request)
    categoria = request.POST.get('categoria','')
    producto_seleccionado = request.POST.get('producto_seleccionado','')
    region_provincia_seleccionada = request.POST.get('producto_seleccionado','')
    tipo_producto = TipoProductos.objects.get(numero=int(categoria))
    ofertas_productos = ProductoEnOferta.objects.filter(producto__tipo_producto = tipo_producto)
    ofertas_mapa = []
    dict_categorias = {}
    dict_categorias[int(categoria)] = tipo_producto.nombre
    dict_ofertas = {}
    es_post = [{'es_post':1}]
    ofertas_vistas = []
    tips = Tips.objects.all()
    
    for op in ofertas_productos:
        producto_oferta = ProductoEnOferta.objects.filter(oferta=op.oferta)
        try:
            dict_ofertas[op.oferta]
        except:
            dict_ofertas[op.oferta] = producto_oferta

        try:
            #foto_empresa_path = op.oferta.sucursal.empresa.foto_empresa.url
            foto_empresa_path = '/static/media/fotos_empresas/' + op.oferta.sucursal.empresa.filename()
	    foto_empresa_path = foto_empresa_path.split('/')
            foto_empresa_path = foto_empresa_path[len(foto_empresa_path-1)]

        except:
            foto_empresa_path = ''

        if op.oferta not in ofertas_vistas:
            ofertas_tmp = {'direccion': op.oferta.sucursal.direccion,'razon_social': op.oferta.sucursal.nombre,'latitud': op.oferta.sucursal.latitud,'longitud': op.oferta.sucursal.longitud,'id_oferta': op.oferta.id,'foto_empresa':foto_empresa_path}
            ofertas_mapa.append(ofertas_tmp)
            ofertas_vistas.append(op.oferta)
    print '------->', tips
    for i in tips:
        tips_tmp = {'direccion': i.direccion,'razon_social': i.nombre,'latitud': i.latitud,'longitud': i.longitud,'id_oferta': i.id,'foto_empresa':'' , 'type': 'tips'}
        ofertas_mapa.append(tips_tmp)

    
    numero_resultados = len(ofertas_vistas)

    tiempos = [{'year_fin': op.oferta.fecha_fin.year,'month_fin': op.oferta.fecha_fin.month,'day_fin': op.oferta.fecha_fin.day, 'hour_fin': op.oferta.fecha_fin.hour,'minute_fin': op.oferta.fecha_fin.minute, 'id_oferta': op.oferta.id} for op in ofertas_productos]
    data = {
        'ofertas':dict_ofertas,
        'categorias':dict_categorias,
        'usuario':usuario,
        'numero_resultados':numero_resultados,
        'tiempos':json.dumps(tiempos),
        'ofertas_mapa':json.dumps(ofertas_mapa),
        'es_post':json.dumps(es_post),
        'region_provincia_seleccionada':region_provincia_seleccionada,
        'producto_seleccionado': producto_seleccionado
    }
    return render(request, 'interior.html', data)

def vermas(request):
    id_oferta = request.GET.get('oferta')
    oferta = Oferta.objects.get(id=int(id_oferta))
    lista_productos = ProductoEnOferta.objects.filter(oferta=oferta)
    oferta.count=oferta.count+1
    oferta.save()
    data = {
        'oferta':oferta,
        'lista_productos':lista_productos
    }
    return render(request, 'modal_vermas.html', data)

def cotizar(request):
    return render(request, 'interior.html', data)

def listar_productos(request):
    nombre_producto = request.GET.get('term','')
    productos = Producto.objects.filter(nombre__icontains=nombre_producto).order_by('nombre')
    tipoProductos = TipoProductos.objects.filter(nombre__icontains=nombre_producto).order_by('nombre')
    lista_productos = []
    
    for ps in productos:
        lista_productos.append(ps.nombre)    
    for ps in tipoProductos:
        lista_productos.append(ps.nombre)
        
    return JsonResponse(lista_productos, safe=False)

def listar_regiones(request):
    nombre = request.GET.get('term','')
    regiones = Region.objects.filter(nombre__icontains=nombre)
    provincias = Provincia.objects.filter(nombre__icontains=nombre)
    lista_resultado = []
    
    for r in regiones:
        lista_resultado.append(r.nombre)
    for p in provincias:
        lista_resultado.append(p.nombre)
    return JsonResponse(lista_resultado, safe=False)

@csrf_exempt
def envia_cotizacion(request):
    usuario = chequea_usuario(request)
    productor = Productor.objects.get(username__icontains=usuario)
    id_oferta = request.POST.get('id-oferta','')
    lista_productos_cotizar = request.POST.getlist('producto-cotizar')
    cantidad_cotizada = {}
    for lpc in lista_productos_cotizar:
        peo = ProductoEnOferta.objects.get(id=int(lpc))
        cantidad = request.POST.get('unidades-prod-'+lpc)
        if cantidad == '':
            continue
        cantidad_cotizada[peo] = cantidad
        asociacion = AsociacionOfertaProductoCotizacion.objects.create(
            oferta = peo.oferta,
            producto = peo.producto,
            cantidad_cotizada = cantidad
            ) 
        CotizacionProductorOferta.objects.create(
        asociacion_cotizacion_productor = asociacion,
        productor=productor
        )

    oferta = Oferta.objects.get(id=int(id_oferta))
    empresa_razon_social = productor.empresa.razon_social
    empresa_rut = productor.empresa.rut[:-1]+'-'+productor.empresa.rut[-1:]
    productos_oferta = ProductoEnOferta.objects.filter(oferta=oferta)

    now = datetime.now()
    fecha_junta = str(now.year)+str(now.month).zfill(2)+str(now.day).zfill(2)+'_'+str(now.hour).zfill(2)+str(now.minute).zfill(2)+str(now.second).zfill(2)
    nombre_archivo_cotizacion = PATH_CORREOS_ENVIADOS+'/cotizacion/cotizacion_'+fecha_junta+'.html'

    nombre_archivo_cotizacion = PATH_CORREOS_ENVIADOS+'/mail-cotizacion-buscagro.html'
    data_mail = {
        'usuario': usuario,
        'empresa_razon_social': empresa_razon_social,
        'empresa_rut': empresa_rut,
        'productos_cotizados': cantidad_cotizada,
        'productos_oferta': productos_oferta,
        'empresa_oferente': oferta.sucursal,
        'condiciones': oferta.texto_despacho
    }

    response = render_to_string(PATH_CORREOS+'/mail-cotizacion-buscagro.html', data_mail)
    from django.core.mail import send_mail
    from django.core.mail import EmailMultiAlternatives
    
    import smtplib
    from email.mime.text import MIMEText

    # Define to/from
    #sender = 'sender@example.com'
    #recipient = 'narbelys@buscagro.cl'

    # Create message
    #msg = MIMEText("Message text")
    #msg['Subject'] = "Sent from python"
    #msg['From'] = sender
    #msg['To'] = recipient

    # Create server object with SSL option
    #server = smtplib.SMTP_SSL('smtp.zoho.com', 465)

    # Perform operations via server
    #server.login('narbelys@buscagro.cl', 'narbelys')
    #server.sendmail(sender, [recipient], msg.as_string())
    #server.quit()






    import smtplib
    import email.utils
    from email.mime.text import MIMEText
    from email.mime.multipart import MIMEMultipart
    # Create the message
    msg = MIMEMultipart('alternative')
    msg['To'] = email.utils.formataddr((productor.username, productor.email))
    msg['From'] = email.utils.formataddr(('Contacto', 'narbelys@buscagro.cl'))
    msg['Subject'] = oferta.sucursal.empresa.email_vendedor
    #part2 = MIMEText(response, 'html')
    part2 = MIMEText(response.encode('utf-8'), 'html', 'utf-8')
    msg.attach(part2)


    server = smtplib.SMTP_SSL('smtp.zoho.com', 465)
    server.set_debuglevel(True) # show communication with the server
    server.login('narbelys@buscagro.cl','narbelys')
    try:
       server.sendmail('narbelys@buscagro.cl', ['narbelys@buscagro.cl','contacto@buscagro.cl', productor.email], msg.as_string())
    finally:
       server.quit()




    subject = oferta.sucursal.empresa.email_vendedor

    msg = EmailMultiAlternatives(subject, response, 'contacto@buscagro.cl', ['narbelys@buscagro.cl','narbelys@gmail.com', 'contacto@buscagro.cl', productor.email])
    #msg = EmailMultiAlternatives(subject, response, 'contacto@buscagro.cl', ['narbelys@buscagro.cl'])
    msg.attach_alternative(response, "text/html")
    #msg.send()
    #msg.quit()

  


   
    with open(nombre_archivo_cotizacion, 'w') as static_file:
        static_file.write(response)

    os.system("/usr/bin/mutt -e \"set content_type=text/html\" -e \"set from='Buscagro <contacto@buscagro.cl>'\" '" + productor.email + "' -c '"+ oferta.sucursal.empresa.email_vendedor +"' -c 'contacto@buscagro.cl' -s 'Cotizacion de Productos' < "+nombre_archivo_cotizacion)

    #os.system("/usr/bin/mutt -e \"set content_type=text/html\" -e \"set from='Buscagro <contacto@buscagro.cl>'\" 'grungero@gmail.com' -s 'Cotizacion de Productos' < "+nombre_archivo_cotizacion)
    dict_ofertas = {}
    o = oferta
    dict_ofertas[o] = productos_oferta
    numero_resultados = productos_oferta.count()
    tiempos = [{'year_fin': o.fecha_fin.year,'month_fin': o.fecha_fin.month,'day_fin': o.fecha_fin.day, 'hour_fin': o.fecha_fin.hour,'minute_fin': o.fecha_fin.minute, 'id_oferta': o.id}]
    ofertas_mapa = []
    ofertas_tmp = {'razon_social': oferta.sucursal.nombre,'latitud': oferta.sucursal.latitud,'longitud': oferta.sucursal.longitud,'id_oferta': oferta.id}
    ofertas_mapa.append(ofertas_tmp)
    es_post = [{'es_post':1}]
    data = {
        'ofertas':dict_ofertas,
        'usuario':usuario,
        'numero_resultados':numero_resultados,
        'tiempos':json.dumps(tiempos),
        'ofertas_mapa':json.dumps(ofertas_mapa),
        'es_post':json.dumps(es_post)
    }
    return render(request, 'interior.html', data)    
