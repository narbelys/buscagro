from django.contrib import admin
from oferta.models import *
import datetime
from django.http import HttpResponse
import csv
from django.utils import timezone
# Register your models here.


def export_as_csv_action(description="Export selected objects as CSV file",
                         fields=None, exclude=None, header=True):

    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """
    def export_as_csv(modeladmin, request, queryset):
        """
        Generic csv export admin action.
        based on http://djangosnippets.org/snippets/1697/
        """
        opts = modeladmin.model._meta
        field_names = set([field.name for field in opts.fields])
        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset
        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset

        today = datetime.datetime.now()
        today = "__%s/%s/%s" % (today.day, today.month, today.year) + \
                "__%sh%sm%ss" % (today.hour, today.month, today.second)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s' % \
                    unicode(opts).replace('.', '_') + today + '.csv'
        writer = csv.writer(response)
        if header:
            array=list(field_names)
            array = array + ['usuario', 'empresa_razon_social', 'empresa_rut', 'cantidad', 'productos_oferta', 'empresa_oferente', 'condiciones'] 
            writer.writerow(array)

        for obj in queryset:
            row = []
            for field in field_names:
                #if 'datetime.datetime' in str(type(getattr(obj, field))):
                #    row = row + [getattr(obj, field)
                #                    .astimezone(timezone('America/Santiago'))]
                #else:
                row = row + [unicode(getattr(obj, field)).encode('utf-8')]
            print obj.productor
            username = obj.productor.username
            razon_social = obj.productor.empresa.razon_social
            producto_oferta = ProductoEnOferta.objects.filter(oferta=obj.asociacion_cotizacion_productor.oferta.pk)
            if producto_oferta: 
                producto_oferta = producto_oferta[0]
            else: 
                producto_oferta = ''
            row = row+ [username , razon_social , obj.productor.empresa.rut[:-1]+'-'+obj.productor.empresa.rut[-1:], obj.asociacion_cotizacion_productor.cantidad_cotizada, producto_oferta,  obj.asociacion_cotizacion_productor.oferta.sucursal, obj.asociacion_cotizacion_productor.oferta.texto_despacho]
            writer.writerow(row)
        return response
    export_as_csv.short_description = description
    return export_as_csv

class CotizacionProductorOfertaAdmin(admin.ModelAdmin):
    list_display = ['fecha', 'owner', 'name', 'producto', 'cantidad']
    actions = [export_as_csv_action("Exportar a csv",
               fields=['fecha', 'owner'],
               header=True)]
    
    def owner(self, obj):
        return obj.productor.empresa.razon_social    
    def name(self, obj):
        return obj.asociacion_cotizacion_productor.oferta.nombre    
    def producto(self, obj):
        return obj.asociacion_cotizacion_productor.producto.nombre    
    def cantidad(self, obj):
        return obj.asociacion_cotizacion_productor.cantidad_cotizada
    

    
admin.site.register(ProductoEnOferta)
admin.site.register(Oferta)
admin.site.register(AsociacionOfertaProductoCotizacion)
admin.site.register(CotizacionProductorOferta, CotizacionProductorOfertaAdmin)
#admin.site.register(CotizacionProductorOferta)
