# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0003_sucursal'),
        ('oferta', '0003_auto_20151202_1708'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='oferta',
            name='empresa',
        ),
        migrations.AddField(
            model_name='oferta',
            name='sucursal',
            field=models.ForeignKey(related_name='oferta', default='', to='registro.Sucursal'),
            preserve_default=False,
        ),
    ]
