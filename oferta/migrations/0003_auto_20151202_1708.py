# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('oferta', '0002_auto_20151112_0253'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='oferta',
            options={'verbose_name_plural': 'Oferta'},
        ),
    ]
