# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('oferta', '0004_auto_20160110_2224'),
    ]

    operations = [
        migrations.AddField(
            model_name='cotizacionproductoroferta',
            name='fecha',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
