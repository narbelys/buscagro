# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AsociacionOfertaProductoCotizacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cantidad_cotizada', models.PositiveIntegerField(default=0)),
            ],
            options={
                'verbose_name_plural': 'Asociacion Cotizacion Producto Oferta',
            },
        ),
        migrations.CreateModel(
            name='CompraProductorLiquidacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('asociacion_cotizacion_productor', models.ForeignKey(to='oferta.AsociacionOfertaProductoCotizacion')),
                ('productor', models.ForeignKey(to='registro.Productor')),
            ],
            options={
                'verbose_name_plural': 'Cotizacion Oferta - Productor',
            },
        ),
        migrations.CreateModel(
            name='Oferta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=200, null=True, blank=True)),
                ('ahorro', models.PositiveIntegerField(default=0)),
                ('texto_despacho', models.CharField(max_length=500, null=True, blank=True)),
                ('texto_lateral_metodo_pago', models.CharField(max_length=500, null=True, blank=True)),
                ('texto_lateral_metodo_entrega', models.CharField(max_length=500, null=True, blank=True)),
                ('texto_lateral_empresa_oferente', models.CharField(max_length=500, null=True, blank=True)),
                ('nombre_sucursal', models.CharField(max_length=500, null=True, blank=True)),
                ('direccion_sucursal', models.CharField(max_length=500, null=True, blank=True)),
                ('telefono_sucursal', models.CharField(max_length=500, null=True, blank=True)),
                ('fecha_inicio', models.DateTimeField()),
                ('fecha_fin', models.DateTimeField()),
                ('activa', models.BooleanField(default=True)),
                ('empresa', models.ForeignKey(related_name='oferta', to='registro.Empresa')),
                ('region', models.ForeignKey(related_name='oferta', to='registro.Region')),
            ],
            options={
                'verbose_name_plural': 'Liquidacion',
            },
        ),
        migrations.CreateModel(
            name='ProductoEnOferta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('porcentaje_descuento', models.IntegerField()),
                ('envoltorio', models.CharField(max_length=200, choices=[(b'-', b'-'), (b'0,25 Lt', b'0,25 Lt'), (b'1 Lt', b'1 Lt'), (b'3,8 Lt', b'3,8 Lt'), (b'5 Lt', b'5 Lt'), (b'10 Lt', b'10 Lt'), (b'20 Lt', b'20 Lt'), (b'25 Lt', b'25 Lt'), (b'205 Lt', b'205 Lt'), (b'0,5 Kg', b'0,5 Kg'), (b'1 Kg', b'1 Kg'), (b'5 Kg', b'5 Kg'), (b'10 Kg', b'10 Kg'), (b'18 Kg', b'18 Kg'), (b'20 Kg', b'20 Kg'), (b'25 Kg', b'25 Kg'), (b'50 Kg', b'50 Kg')])),
                ('descripcion', models.CharField(max_length=500, null=True, blank=True)),
                ('oferta', models.ForeignKey(to='oferta.Oferta')),
                ('producto', models.ForeignKey(to='registro.Producto')),
            ],
            options={
                'verbose_name_plural': 'Productos en la Oferta',
            },
        ),
        migrations.AddField(
            model_name='asociacionofertaproductocotizacion',
            name='oferta',
            field=models.ForeignKey(to='oferta.Oferta'),
        ),
        migrations.AddField(
            model_name='asociacionofertaproductocotizacion',
            name='producto',
            field=models.ForeignKey(to='registro.Producto'),
        ),
    ]
