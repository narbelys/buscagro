from django.db import models
from registro.models import *


class Oferta(models.Model):
    nombre = models.CharField(max_length=200, blank=True, null=True)
    region = models.ForeignKey(Region, related_name='oferta')
    ahorro = models.PositiveIntegerField(default=0)
    sucursal = models.ForeignKey(Sucursal, related_name="oferta")
    texto_despacho = models.CharField(max_length=500, blank=True, null=True ,  verbose_name="Condiciones de venta")
    texto_lateral_metodo_pago = models.CharField(max_length=500, blank=True, null=True)
    texto_lateral_metodo_entrega = models.CharField(max_length=500, blank=True, null=True)
    texto_lateral_empresa_oferente = models.CharField(max_length=500, blank=True, null=True)
    nombre_sucursal = models.CharField(max_length=500, blank=True, null=True)
    direccion_sucursal = models.CharField(max_length=500, blank=True, null=True)
    telefono_sucursal = models.CharField(max_length=500, blank=True, null=True)
    fecha_inicio = models.DateTimeField()
    fecha_fin = models.DateTimeField()
    count = models.PositiveIntegerField(default=0)
    #producto_stock = models.ManyToManyField('ProductoStock', blank=True, null=True, related_name="oferta_stock")
    activa = models.BooleanField(default=True)

    def __unicode__(self):
        return self.nombre #+ ' - ' + self.region.nombre
     
    class Meta:
        verbose_name_plural = "Oferta"  

class ProductoEnOferta(models.Model):
    oferta = models.ForeignKey(Oferta)
    #compra_producto_cantidad = models.ManyToManyField('AsociacionofertaProductoStock')
    producto = models.ForeignKey(Producto)
    porcentaje_descuento = models.IntegerField()
    envoltorio = models.CharField(max_length=200, choices = envoltorios)
    descripcion = models.CharField(max_length=500, blank=True, null=True)
        
    def __unicode__(self):
        return self.oferta.nombre + " - " + self.producto.nombre
     
    class Meta:
        verbose_name_plural = "Productos en la Oferta"
        #order_with_respect_to = 'producto'  
        
class AsociacionOfertaProductoCotizacion(models.Model):
    oferta = models.ForeignKey(Oferta)
    producto = models.ForeignKey(Producto)
    cantidad_cotizada = models.PositiveIntegerField(default=0)
    
    def __unicode__(self):
        return self.oferta.nombre + " - " + self.producto.nombre + " - " + str(self.cantidad_cotizada)
     
    class Meta:
        verbose_name_plural = "Asociacion Cotizacion Producto Oferta"
        
class CotizacionProductorOferta(models.Model):
    asociacion_cotizacion_productor = models.ForeignKey(AsociacionOfertaProductoCotizacion)
    fecha = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    #oferta = models.ForeignKey(oferta)
    #productos = models.CharField(max_length=500)
    #cantidades = models.CharField(max_length=200)
    productor = models.ForeignKey(Productor)
    
    def __unicode__(self):
        return self.productor.username + " - " + self.asociacion_cotizacion_productor.oferta.nombre + " - " + self.asociacion_cotizacion_productor.producto.nombre + " - " + str(self.asociacion_cotizacion_productor.cantidad_cotizada)
     
    class Meta:
        verbose_name_plural = "Cotizacion Oferta - Productor" 
