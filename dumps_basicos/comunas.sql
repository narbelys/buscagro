-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: buscagro
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `registro_comuna`
--

DROP TABLE IF EXISTS `registro_comuna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_comuna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `provincia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_comuna_54bf7e76` (`provincia_id`),
  CONSTRAINT `registro_comuna_provincia_id_33406e7c_fk_registro_provincia_id` FOREIGN KEY (`provincia_id`) REFERENCES `registro_provincia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_comuna`
--

LOCK TABLES `registro_comuna` WRITE;
/*!40000 ALTER TABLE `registro_comuna` DISABLE KEYS */;
INSERT INTO `registro_comuna` VALUES (3,'Calle Larga',5),(4,'Los Andes',5),(5,'Rinconada',5),(6,'San Esteban',5),(7,'Cabildo',6),(8,'La Ligua',6),(9,'Papudo',6),(10,'Petorca',6),(11,'Zapallar',6),(12,'Hijuelas',7),(13,'La Calera',7),(14,'La Cruz',7),(15,'Nogales',7),(16,'Quillota',7),(17,'Catemu',8),(18,'Llay-Llay',8),(19,'Panquehue',8),(20,'Putaendo',8),(21,'San Felipe',8),(22,'Santa María',8),(23,'Limache',9),(24,'Olmué',9),(25,'Quilpué',9),(26,'Villa Alemana',9),(27,'Colina',10),(28,'Lampa',10),(29,'Tiltil',10),(30,'Pirque',11),(31,'Puente Alto',11),(32,'San José de Maipo',11),(33,'Buin',12),(34,'Calera de Tango',12),(35,'Paine',12),(36,'San Bernardo',12),(37,'Alhué',13),(38,'Curacaví',13),(39,'María Pinto',13),(40,'Melipilla',13),(41,'San Pedro',13),(42,'El Monte',14),(43,'Isla de Maipo',14),(44,'Padre Hurtado',14),(45,'Peñaflor',14),(46,'Talagante',14),(47,'Codegua',15),(48,'Coinco',15),(49,'Coltauco',15),(50,'Doñihue',15),(51,'Graneros',15),(52,'Las Cabras',15),(53,'Machalí',15),(54,'Malloa',15),(55,'Mostazal',15),(56,'Olivar',15),(57,'Peumo',15),(58,'Pichidegua',15),(59,'Quinta de Tilcoco',15),(60,'Rancagua',15),(61,'Rengo',15),(62,'Requínoa',15),(63,'San Vicente de Tagua Tagua',15),(64,'La Estrella',16),(65,'Litueche',16),(66,'Marchihue',16),(67,'Navidad',16),(68,'Paredones',16),(69,'Pichilemu',16),(70,'Chépica',17),(71,'Chimbarongo',17),(72,'Lolol',17),(73,'Nancagua',17),(74,'Palmilla',17),(75,'Peralillo',17),(76,'Placilla',17),(77,'Pumanque',17),(78,'San Fernando',17),(79,'Santa Cruz',17),(80,'Curicó',18),(81,'Hualañé',18),(82,'Licantén',18),(83,'Molina',18),(84,'Rauco',18),(85,'Romeral',18),(86,'Sagrada Familia',18),(87,'Teno',18),(88,'Vichuquén',18),(89,'Colbún',19),(90,'Linares',19),(91,'Longaví',19),(92,'Parral',19),(93,'Retiro',19),(94,'San Javier',19),(95,'Villa Alegre',19),(96,'Yerbas Buenas',19),(97,'Constitución',20),(98,'Curepto',20),(99,'Empedrado',20),(100,'Maule',20),(101,'Pelarco',20),(102,'Pencahue',20),(103,'Río Claro',20),(104,'San Clemente',20),(105,'San Rafael',20),(106,'Talca',20);
/*!40000 ALTER TABLE `registro_comuna` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-11 23:20:49
