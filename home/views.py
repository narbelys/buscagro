import os
from django.shortcuts import render, render_to_response, redirect
from django.template.context import RequestContext
from django.http import HttpResponseRedirect, HttpResponse,\
                        HttpResponseNotAllowed, HttpResponseBadRequest, \
                        HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
import json
from registro.models import *
from home.utils import creaCorreoRecuperarClave,creaCorreoRegistroRapido
from django.core.mail import send_mail

# Create your views here.

def chequea_usuario(request):
	try:
		usuario = request.session['productor_id']
	except Exception as e:
		print e
		usuario = ''
	return usuario

def home(request):
	regiones = Region.objects.all()
	usuario = chequea_usuario(request)

	data = {'mostrar':'no',
			'existe':'no',
			'regiones':regiones,
			'usuario':usuario
			}
	return render(request,'index.html',data)

@csrf_exempt
def login(request):
    print 'user'
    username = request.POST['usuario']
    password = request.POST['password']
    try:
        productor = Productor.objects.get(username=username)
        request.session['productor_id'] = username
        usuario = username
    except Productor.DoesNotExist:
        usuario = ''
    except KeyError:
        usuario = ''

    data = {'mostrar':'no',
			'existe':'no',
			'usuario': usuario
			}
    return render(request,'index.html',data)

def logout(request):
	try:
		del request.session['productor_id']
	except KeyError:
		pass
	usuario = ''
	data = {'mostrar':'no',
			'existe':'no',
			'usuario': usuario
	}
	return render(request,'index.html',data)

@csrf_exempt
def guardar_email(request):
	email = request.POST['email']
	mostrar = 'si'
	existe = 'no'
	try:
		Usuario.objects.get(email=email)
		mostrar = 'no'
		existe = 'si'
	except Usuario.DoesNotExist:
		Usuario.objects.create(email=email)
		path_html = creaCorreoRegistroRapido()
		
		os.system("/usr/bin/mutt -e \"set content_type=text/html\" -e \"set from='Buscagro <contacto@buscagro.cl>' \" '" + email + "' -s 'Registro Buscagro' < "+path_html)

	data = {'mostrar':mostrar,
			'existe':existe}
	c = RequestContext(request, data)
	return render_to_response('index.html',c)

@csrf_exempt
def enviar_clave(request):
    email = request.POST['email']
    p = ()
    try:
        p = Productor.objects.get(email = email)
    except Exception,e:
        return render(request,'index.html',data)
    
    path_html = creaCorreoRecuperarClave(p.username,p.password )
    
    os.system("/usr/bin/mutt -e \"set content_type=text/html\" -e \"set from='Buscagro <contacto@buscagro.cl>' \" '" + p.email + "' -s 'Recuperacion Clave Secreta Buscagro' < "+path_html)
    regiones = Region.objects.all()
    data = {'mostrar':'no',
            'existe':'no',
            'regiones':regiones,
            'usuario':''
           }

    return render(request,'index.html',data)

@csrf_exempt
def contacto(request):
	nombre = request.POST.get('nombre')
	telefono = request.POST.get('telefono')
	email = request.POST.get('email')
	mensaje = request.POST.get('mensaje')

	os.system("echo '"+mensaje+"' | /usr/bin/mutt -e \"set from='"+nombre+" <"+email+">' \" ' contacto@buscagro.cl ' -s 'Cliente lo ha contactado' ")
	return redirect('home')

def buscar(request):
	usuario = chequea_usuario(request)
	data = {'mostrar':'no',
			'existe':'no',
			'usuario':usuario
			}
	c = RequestContext(request, data)
	return render_to_response('interior.html',c)

def quienes_somos(request):
	regiones = Region.objects.all()
	usuario = chequea_usuario(request)
	data = {'mostrar':'no',
			'existe':'no',
			'regiones':regiones,
			'usuario':usuario
			}
	c = RequestContext(request, data)
	return render_to_response('quienes-somos.html',c)

def preguntas_frecuentes(request):
	regiones = Region.objects.all()
	usuario = chequea_usuario(request)
	print usuario
	data = {'mostrar':'no',
			'existe':'no',
			'regiones':regiones,
			'usuario':usuario
			}
	c = RequestContext(request, data)
	return render_to_response('preguntas-frecuentes.html',c)

def politicas(request):
	regiones = Region.objects.all()
	usuario = chequea_usuario(request)
	data = {'mostrar':'no',
			'existe':'no',
			'regiones':regiones,
			'usuario':usuario
			}
	c = RequestContext(request, data)
	return render_to_response('politicas.html',c)

def terminos_condiciones(request):
	regiones = Region.objects.all()
	usuario = chequea_usuario(request)
	data = {'mostrar':'no',
			'existe':'no',
			'regiones':regiones,
			'usuario':usuario
			}
	c = RequestContext(request, data)
	return render_to_response('terminos-y-condiciones.html',c)

def sendmail(request):
    user = request.POST.get('user',None)
    aporte = request.POST.get('aporte',None)
    print user, aporte
    #return HttpResponse(json.dumps({'status':'Error'}), content_type = "application/json")
    #send_mail('Buscagro: Duda de '+user, aporte, 'shoes@example.com',['narbelys@gmail.com'], fail_silently=False)
    
    #try:
    import smtplib
    import email.utils
    from email.mime.text import MIMEText

    # Create the message
    msg = MIMEText(aporte)
    msg['To'] = email.utils.formataddr(('Contacto', 'narbelys@buscagro.cl'))
    msg['From'] = email.utils.formataddr((user, 'narbelys@buscagro.cl'))
    msg['Subject'] = 'Buscagro: Duda de '+user


    server = smtplib.SMTP_SSL('smtp.zoho.com', 465)
    server.set_debuglevel(True) # show communication with the server
    server.login('narbelys@buscagro.cl','narbelys')
    try:
        server.sendmail('narbelys@buscagro.cl', ['narbelys@buscagro.cl', 'contacto@buscagro.cl'], msg.as_string())
    finally:
        server.quit()

    #send_mail('Buscagro: Duda de '+user, aporte, 'buscagro@consulta.cl' ,['narbelys@buscagro.cl'], fail_silently=False)
    return HttpResponse(json.dumps({'status':'OK'}), content_type = "application/json")
    #except: 
    #    return HttpResponse(json.dumps({'status':'Error'}), content_type = "application/json")
        
