# -*- encoding: utf-8 -*-
import os
from datetime import datetime
from django.template.loader import render_to_string
import cStringIO as StringIO
import json
from registro.models import *

CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
TEMPLATE_DIR = os.path.join(CURRENT_DIR, '..','templates')
PATH_CORREOS = os.path.join(CURRENT_DIR,'..','templates','correos')
PATH_CORREOS_ENVIADOS = os.path.join(CURRENT_DIR,'..','templates','correos_enviados')

def creaCorreoRecuperarClave(usuario, clave):
    now = datetime.now()
    fecha_junta = str(now.year)+str(now.month)+str(now.day)+'_'+str(now.hour)+str(now.minute)+str(now.second)
    nombre_archivo_clave_usuario = PATH_CORREOS_ENVIADOS+'/clave/clave_'+fecha_junta+'.html'
    response = render_to_string(PATH_CORREOS+'/mail-recuperarclave-buscagro.html', 
                                { 'usuario': usuario,
                                 'password': clave })
                    
    with open(nombre_archivo_clave_usuario, 'w') as static_file:
        static_file.write(response)
    
    return nombre_archivo_clave_usuario

def creaCorreoRegistroRapido():
    now = datetime.now()
    fecha_junta = str(now.year)+str(now.month)+str(now.day)+'_'+str(now.hour)+str(now.minute)+str(now.second)
    nombre_archivo_registro_rapido_usuario = PATH_CORREOS_ENVIADOS+'/registro_rapido/registro_rapido_'+fecha_junta+'.html'
    response = render_to_string(PATH_CORREOS+'/mail-registrorapido-buscagro.html', 
                                { 'test': 'test' })
                    
    with open(nombre_archivo_registro_rapido_usuario, 'w') as static_file:
        static_file.write(response)
    
    return nombre_archivo_registro_rapido_usuario
