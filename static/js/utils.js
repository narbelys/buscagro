function autocomplete_input(clase,url){

    var resultado = 1
    $('.'+clase).autocomplete({
        minLength: 2,
        source: url,
        open: function(event, ui) {
            $(".ui-autocomplete").css("z-index", 1100); 
        },
        change: function(event,ui)
        {
        if (ui.item==null)
            {
            $(this).val('');
            $(this).focus();
            }
        }
    })
    return resultado
}



$(document).ready(function(){
    autocomplete_input('lista-productos',"/lista_productos/");
    autocomplete_input('lista-regiones',"/lista_regiones/");

    $('body').on('click','.modal-ver-mas', function(){
        if($(this).hasClass('no-esta-logeado')){
            alert("Debe iniciar sesión para ver las ofertas")
            return false;
        }
    })

    $('.boton_buscar_por_categoria').click(function(){
        id_categoria = $(this).attr('id')
        $('#formulario-buscar-categoria-'+id_categoria).submit()
    })
});