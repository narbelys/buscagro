diccionario_error={
	'razon-social-vacio':'Debe ingresar un valor en la razon social',
	'rut-empresa-vacio':'Debe ingresar un valor en el rut de la empresa',
	'rut-empresa-erroneo':'Rut invalido.',
	'telefono-vacio': 'Debe ingresar un valor en el campo telefono',
	'direccion-vacio': 'Debe ingresar un valor en el campo direccion',
	'usuario-vacio':'Debe ingresar un valor en el campo usuario',
	'password-vacio':'Debe ingresar un valor en el campo password',
	'h-frutales-vacio':'Debe ingresar hectareas de frutales',
	'h-semillas-vacio':'Debe ingresar hectareas de semillas',
}
function field_not_empty(tag){
	if($('input[name="'+tag+'"]').val()==""){
		return "-vacio";
	}
}
function validaciones_registro(){
	exito = 1;
	$('#myModal :input[type="text"]').each(function(){
		tag = $(this).attr('name')
		if(tag != 'h-frutales' || tag != 'h-semillas' ){
			resultado = field_not_empty(tag)
			if(resultado == '-vacio'){
				exito = 0;
				alert(diccionario_error[tag+resultado])
				return false
			}
		}
		
	})
	return exito
}

function validaciones_registro(){
	exito = 1;
	$('#myModal9 :input[type="text"]').each(function(){
		tag = $(this).attr('name')
		if(tag != 'h-frutales' || tag != 'h-semillas' ){
			resultado = field_not_empty(tag)
			if(resultado == '-vacio'){
				exito = 0;
				alert(diccionario_error[tag+resultado])
				return false
			}
		}
		
	})
	return exito
}

function valida_rut(){
	
	$('.rut_validador').Rut({
		on_error: function(){ alert('Rut incorrecto'); },
		on_success: function(){ 
			$.post('verifica_existencia_rut/',
				{'rut': $('#rut-empresa').val(),
				},
				function(exito){
					
					if(exito<1){
						alert("Rut ya existe en el sistema")
						$('.rut-empresa').val("")
						$('.rut-empresa').focus()
					}
											
				})},
		format_on: 'keyup'
	});
}




$(document).ready(function(){
	valida_rut()
	$('body').on('click','.boton-registrar',function(){
		var data = $('.registro-form').serialize();
		var url = "crearegistro/";
        if(validaciones_registro() == 0){
        	return false;
        }
        $.post(
            url,
            data, function(data){
            	if(data < 1 || data > 1){
            		alert("Ocurrio un error en el registro")
            		return false;
            	}
            	else 
            		alert("Registro exitoso")
            		
            		//window.location.href = 'http://www.buscagro.cl'
            }).done(function(){
                
                //window.location.href = 'http://www.buscagro.cl'
                
            })

	})


	$(document).on('change','.registro-regiones',function(){
		region = $(this).val()
		
		if(region != 0){
			$.post('/loadprovincias/',
					{'region': region,
					},
					function(data){
						$('.listado-provincias').html(data);						
					})	
		}
		
	});
	
	/*************************************************
	 ********  Al seleccionar una provincia  ************
	 *************************************************/
	$(document).on('change', '.registro-provincias', function(){
		provincia = $(this).val()
		
		if(provincia != 0){
			$.post('/loadcomunas/',
					{'provincia': provincia,
					},
					function(data){
						$('.listado-comunas').html(data);						
					})
			
		}
		});
});
