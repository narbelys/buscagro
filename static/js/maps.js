function init(lista_empresas_mapa){
    var geocoder = new google.maps.Geocoder();
    var address = '';
    var lat = -33.73413
    var lon = -70.73383
    geocoder.geocode({ 'address': 'address' }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var lat2 = results[0].geometry.location.lat();
                    var lon3 = results[0].geometry.location.lng();

                } else {
                    alert("Request failed.")
                }
            });

    var centro = new google.maps.LatLng(lat, lon);
    var propiedades = {
        center:centro,
        zoom:8,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    }

    mapa = new google.maps.Map(document.getElementById('mapa'), propiedades);

    var marcador, contentString, infowindow;
    for(var i=0;i<lista_empresas_mapa.length;i++){
        var centro_aux = new google.maps.LatLng(lista_empresas_mapa[i].latitud, lista_empresas_mapa[i].longitud);
        //contentString = '<div id="content">'+
        //'<div id="bodyContent">'+
        //'<p>La empresa <b>'+lista_empresas_mapa[i].razon_social+'</b>, tiene ofertas' +
        //'</div>'+
        //'</div>';

        infowindow = new google.maps.InfoWindow({
          content: lista_empresas_mapa[i].razon_social
        });

       if(lista_empresas_mapa[i].type == "tips"){

                 marcador = new google.maps.Marker({position:centro_aux, 
                                               draggable: true, icon: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"})
        }else{
                 marcador = new google.maps.Marker({position:centro_aux,
                                               draggable: true})

        }
	marcador.setMap(mapa)
        //updateModal(lista_empresas_mapa[i])
        google.maps.event.addListener(marcador, 'click', (function(marcador, i) {
                return function() {

                      if(lista_empresas_mapa[i].type == "tips"){
                          infowindow.setContent(
 '<div id="content"><div id="bodyContent" style="width: 150px;    "><div><p style="text-align: center";><b>'+lista_empresas_mapa[i].razon_social+'</b> : '+lista_empresas_mapa[i].direccion+'</p></div> </div></div>'
		    );
                    }else{
                        infowindow.setContent(
                    '<div id="content"><div id="bodyContent" style="width: 180px;    "><div style="text-align: center;"><img src="http://www.buscagro.cl/'+lista_empresas_mapa[i].foto_empresa+'" style="height: 100px;"/></div><div><p style="text-align: center";>' + lista_empresas_mapa[i].razon_social+','+lista_empresas_mapa[i].direccion+'</p></div> <p><a href="#" id="'+lista_empresas_mapa[i].id_oferta+'" class="oferta-mapa prueba">Ver Ofertas</a></p></div></div>'
                        );
                    }

                  //infowindow.setContent(

                   /* '<div id="content"><div id="bodyContent" style="width: 150px;    "><div style="text-align: center;"><img src="http://www.buscagro.cl/'+lista_empresas_mapa[i].foto_empresa+'" style="height: 100px;"/></div><div><p style="text-align: center";>'+lista_empresas_mapa[i].razon_social+','+lista_empresas_mapa[i].direccion+'</p></div> <p><a href="#" id="'+lista_empresas_mapa[i].id_oferta+'" class="oferta-mapa">Ver Ofertas</a></p></div></div>'*/
                    
                    //);
                  infowindow.open(mapa, marcador);
                }
              })(marcador, i));
        //google.maps.event.addListener(marcador, 'click', function() {
        //    infowindow.open(mapa,marcador);        
        //});

    }

}

function muestra_marcadores(productores){
    var geocoder = new google.maps.Geocoder();
    
    for(var i=0;i<productores.length;i++){
        var address = productores[i].direccion;
        var lat = productores[i].latitud
        var lon = productores[i].longitud
        geocoder.geocode({ 'address': 'address' }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var lat2 = results[0].geometry.location.lat();
                        var lon3 = results[0].geometry.location.lng();

                    } else {
                        alert("Request failed.")
                    }
                });

        var centro = new google.maps.LatLng(lat, lon);
        var propiedades = {
            center:centro,
            zoom:15,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        }

        mapa = new google.maps.Map(document.getElementById('mapa'), propiedades);

        var contentString = '<div id="content">'+
        '<div id="bodyContent">'+
        '<p><b>'+productores[i].username+'</b>, tiene '+productores[i].total_hectareas+ ' Hectareas' +
        '</div>'+
        '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });


        var marcador = new google.maps.Marker({position:centro, 
                                               draggable: true})
        marcador.setMap(mapa)
        updateModal(productores[i])
        google.maps.event.addListener(marcador, 'click', function() {
            infowindow.open(mapa,marcador);        
        });

    }   
    
}

function updateModal(productor){
    
    $('.modal-body').text("<div></div>El productor "+productor.username + " tiene "+ productor.total_hectareas)
}




$(document).ready(function(){
    //google.maps.event.addDomListener(window, 'load', init)

    init(lista_empresas_mapa)
    

    $('body').on('click','.oferta-mapa',function(){
        var id_oferta = $(this).attr('id')
        $('.id-oferta-form').val(id_oferta)
        $('#formulario-buscar').submit()
    })

    $()

})
