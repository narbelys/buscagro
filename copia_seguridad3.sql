-- MySQL dump 10.13  Distrib 5.6.29-76.2, for Linux (x86_64)
--
-- Host: localhost    Database: buscagro
-- ------------------------------------------------------
-- Server version	5.6.29-76.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add usuario',7,'add_usuario'),(20,'Can change usuario',7,'change_usuario'),(21,'Can delete usuario',7,'delete_usuario'),(22,'Can add region',8,'add_region'),(23,'Can change region',8,'change_region'),(24,'Can delete region',8,'delete_region'),(25,'Can add provincia',9,'add_provincia'),(26,'Can change provincia',9,'change_provincia'),(27,'Can delete provincia',9,'delete_provincia'),(28,'Can add comuna',10,'add_comuna'),(29,'Can change comuna',10,'change_comuna'),(30,'Can delete comuna',10,'delete_comuna'),(31,'Can add empresa',11,'add_empresa'),(32,'Can change empresa',11,'change_empresa'),(33,'Can delete empresa',11,'delete_empresa'),(34,'Can add productor',12,'add_productor'),(35,'Can change productor',12,'change_productor'),(36,'Can delete productor',12,'delete_productor'),(37,'Can add tipo especie',13,'add_tipoespecie'),(38,'Can change tipo especie',13,'change_tipoespecie'),(39,'Can delete tipo especie',13,'delete_tipoespecie'),(40,'Can add especie',14,'add_especie'),(41,'Can change especie',14,'change_especie'),(42,'Can delete especie',14,'delete_especie'),(43,'Can add productor especie',15,'add_productorespecie'),(44,'Can change productor especie',15,'change_productorespecie'),(45,'Can delete productor especie',15,'delete_productorespecie'),(46,'Can add tipo productos',16,'add_tipoproductos'),(47,'Can change tipo productos',16,'change_tipoproductos'),(48,'Can delete tipo productos',16,'delete_tipoproductos'),(49,'Can add producto',17,'add_producto'),(50,'Can change producto',17,'change_producto'),(51,'Can delete producto',17,'delete_producto'),(52,'Can add oferta',18,'add_oferta'),(53,'Can change oferta',18,'change_oferta'),(54,'Can delete oferta',18,'delete_oferta'),(55,'Can add producto en oferta',19,'add_productoenoferta'),(56,'Can change producto en oferta',19,'change_productoenoferta'),(57,'Can delete producto en oferta',19,'delete_productoenoferta'),(58,'Can add asociacion oferta producto cotizacion',20,'add_asociacionofertaproductocotizacion'),(59,'Can change asociacion oferta producto cotizacion',20,'change_asociacionofertaproductocotizacion'),(60,'Can delete asociacion oferta producto cotizacion',20,'delete_asociacionofertaproductocotizacion'),(61,'Can add cotizacion productor oferta',21,'add_cotizacionproductoroferta'),(62,'Can change cotizacion productor oferta',21,'change_cotizacionproductoroferta'),(63,'Can delete cotizacion productor oferta',21,'delete_cotizacionproductoroferta'),(64,'Can add sucursal',22,'add_sucursal'),(65,'Can change sucursal',22,'change_sucursal'),(66,'Can delete sucursal',22,'delete_sucursal');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) DEFAULT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$20000$153PZGOTAl8r$ccedTwNERqgLzqIU9m4vt+RIgNdWzmnj+v4vFjAHbbw=','2016-01-28 03:31:59',1,'aperez','','','',1,1,'2015-11-12 04:18:11'),(2,'pbkdf2_sha256$24000$BBSmhy8NkzBt$K2ZvkUnjZIyHRLOb8fD06hQgttS83GYVgtU+cfC5h7o=','2016-04-21 04:02:53',1,'buscagro','','','',1,1,'2015-11-12 04:18:25'),(3,'pbkdf2_sha256$24000$862d1dvU8IYD$uRJEYqBUDXPMetDRWfy/0KSFULfT9m2M8geKdLdVyvg=','2016-04-11 20:27:11',1,'narbelys','','','narbelys@gmail.com',1,1,'2016-03-22 20:45:20');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `djang_content_type_id_697914295151027a_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` (`user_id`),
  CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2015-11-12 04:22:25','1','1 - Fertilizantes',1,'',16,1),(2,'2015-11-12 04:22:49','1','producto test 1',1,'',17,1),(3,'2015-11-12 04:22:58','2','producto test 2',1,'',17,1),(4,'2015-11-12 04:26:36','1','Oferta test - Metropolitana',1,'',18,1),(5,'2015-11-12 04:27:13','1','Oferta test - producto test 1',1,'',19,1),(6,'2015-11-12 04:27:32','2','Oferta test - producto test 2',1,'',19,1),(7,'2015-11-12 04:27:55','1','Oferta test - Metropolitana',2,'Changed texto_despacho.',18,1),(8,'2015-11-15 04:14:40','2','Oferta test 2 - Metropolitana',1,'',18,1),(9,'2015-11-15 04:14:53','1','Oferta test - Metropolitana',2,'Changed fecha_fin.',18,1),(10,'2015-11-15 04:15:24','3','Oferta test 2 - producto test 2',1,'',19,1),(11,'2015-11-15 04:16:36','1','grungero',2,'Changed latitud and longitud.',11,1),(12,'2015-11-17 14:36:34','3','test alex - O\'Higgins',1,'',18,2),(13,'2015-11-17 14:37:59','2','2 - Herbicida',1,'',16,2),(14,'2015-11-17 14:39:48','3','Roundap',1,'',17,2),(15,'2015-11-17 14:40:15','4','test alex - Roundap',1,'',19,2),(16,'2015-11-25 16:24:27','2','Alexis',1,'',11,2),(17,'2015-11-25 16:26:38','3','test alex - Metropolitana',2,'Changed region, empresa and fecha_fin.',18,2),(18,'2015-11-25 16:30:34','2','Alexis',2,'Changed latitud and longitud.',11,2),(19,'2015-11-25 16:37:15','2','Alexis',2,'Changed region.',11,2),(20,'2015-11-30 12:55:37','4','test alex - Roundap',2,'Changed envoltorio.',19,2),(21,'2015-11-30 12:57:23','2','Alexis',2,'Changed region.',11,2),(22,'2015-11-30 17:53:48','3','3 - Insecticida',1,'',16,2),(23,'2015-11-30 18:02:13','4','Karate Zeon',1,'',17,2),(24,'2015-11-30 18:02:50','4','4 - Fungicida',1,'',16,2),(25,'2015-11-30 18:03:14','5','5 - Acaracida',1,'',16,2),(26,'2015-11-30 18:03:28','6','6 - Semilla',1,'',16,2),(27,'2015-11-30 18:03:59','7','7 - riego',1,'',16,2),(28,'2015-11-30 18:04:12','8','8 - Vivero',1,'',16,2),(29,'2015-11-30 18:04:32','9','9 - Fitorregulador',1,'',16,2),(30,'2015-11-30 18:04:48','10','10 - Laboratorio',1,'',16,2),(31,'2015-11-30 18:05:02','11','11 - Maquinaria',1,'',16,2),(32,'2015-11-30 18:05:21','12','12 - Herramienta',1,'',16,2),(33,'2015-11-30 18:09:02','3','test alex - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(34,'2015-11-30 18:15:59','3','Coagra',2,'Changed razon_social, telefono, region, provincia, comuna, latitud and longitud.',11,2),(35,'2015-11-30 18:19:28','2','Tattersal',2,'Changed razon_social, telefono, direccion, provincia, comuna, latitud and longitud.',11,2),(36,'2015-11-30 18:23:35','4','Copeval',1,'',11,2),(37,'2015-11-30 18:24:40','1','grungero',2,'No fields changed.',11,2),(38,'2015-11-30 18:26:27','5','Agrocentro',1,'',11,2),(39,'2015-11-30 18:31:23','5','Urea granulada',1,'',17,2),(40,'2015-11-30 18:34:57','6','Tractor 6125D John Deere',1,'',17,2),(41,'2015-11-30 18:37:40','7','Fast Plus',1,'',17,2),(42,'2015-11-30 18:41:38','8','Bravo 720',1,'',17,2),(43,'2015-11-30 18:46:31','9','Semilla Maiz P2069',1,'',17,2),(44,'2015-11-30 18:49:23','5','Agrocentro',2,'No fields changed.',11,2),(45,'2015-11-30 18:49:59','3','test alex - Metropolitana',2,'Changed empresa.',18,2),(46,'2015-11-30 18:53:44','4','copeval - O\'Higgins',1,'',18,2),(47,'2015-11-30 18:54:25','5','copeval - Tractor 6125D John Deere',1,'',19,2),(48,'2015-11-30 19:00:03','5','Agrocentro',2,'No fields changed.',11,2),(49,'2015-11-30 19:15:29','3','Coagra',2,'No fields changed.',11,2),(50,'2015-11-30 19:16:46','5','Coagra - Valparaiso',1,'',18,2),(51,'2015-11-30 19:17:31','6','Coagra - Semilla Maiz P2069',1,'',19,2),(52,'2015-11-30 19:42:24','1','grungero',2,'Changed latitud.',11,1),(53,'2015-12-01 03:13:07','2','alexis',2,'Changed empresa.',12,2),(54,'2015-12-01 03:22:08','2','alexis',2,'Changed empresa.',12,2),(55,'2015-12-01 16:09:21','4','agrocentro',1,'',12,2),(56,'2015-12-01 16:18:31','4','agrocentro',3,'',12,2),(57,'2015-12-01 16:25:32','2','alexis',2,'Changed empresa.',12,2),(58,'2015-12-01 21:00:36','2','alex',2,'Changed empresa.',12,2),(59,'2015-12-01 22:23:06','5','Coagra - Valparaiso',2,'Changed texto_despacho.',18,2),(60,'2015-12-01 22:27:31','5','Coagra - Valparaiso',2,'Changed texto_despacho.',18,2),(61,'2015-12-01 22:31:06','1','grungero',2,'No fields changed.',11,2),(62,'2015-12-03 22:23:52','3','Coagra',2,'Changed es_vendedor and email_vendedor.',11,1),(63,'2015-12-03 22:24:03','4','Copeval',2,'Changed es_vendedor and email_vendedor.',11,1),(64,'2015-12-03 22:24:13','5','Agrocentro',2,'Changed es_vendedor and email_vendedor.',11,1),(65,'2015-12-03 22:24:27','1','grungero',2,'Changed es_vendedor and email_vendedor.',11,1),(66,'2015-12-04 01:21:51','4','Copeval',2,'Changed foto_empresa.',11,2),(67,'2015-12-04 01:25:07','3','Roundap',2,'Changed foto_producto.',17,2),(68,'2015-12-09 01:50:50','5','Coagra - Valparaiso',2,'Changed texto_despacho and fecha_fin.',18,2),(69,'2015-12-09 01:51:20','4','copeval - O\'Higgins',2,'Changed fecha_fin.',18,2),(70,'2015-12-09 01:51:39','3','test alex - Metropolitana',2,'Changed fecha_fin.',18,2),(71,'2015-12-09 01:51:51','2','Oferta test 2 - Metropolitana',2,'No fields changed.',18,2),(72,'2015-12-09 01:51:59','1','Oferta test - Metropolitana',2,'No fields changed.',18,2),(73,'2015-12-09 14:14:14','5','Coagra - Valparaiso',2,'Changed fecha_fin.',18,2),(74,'2015-12-14 20:13:42','9','Semilla Maiz P2069',2,'Changed archivo_pdf.',17,2),(75,'2015-12-14 20:14:23','9','Semilla Maiz P2069',2,'Changed archivo_pdf.',17,2),(76,'2015-12-14 21:01:46','3','Coagra',2,'Changed foto_empresa.',11,2),(77,'2015-12-14 21:06:00','3','Coagra',2,'Changed foto_empresa.',11,2),(78,'2015-12-14 21:06:11','3','Coagra',2,'Changed foto_empresa.',11,2),(79,'2015-12-14 21:07:52','3','Coagra',2,'Changed foto_empresa.',11,2),(80,'2015-12-14 21:08:04','3','Coagra',2,'Changed foto_empresa.',11,2),(81,'2015-12-14 21:17:00','3','Coagra',2,'Changed foto_empresa.',11,2),(82,'2015-12-14 21:17:10','3','Coagra',2,'Changed foto_empresa.',11,2),(83,'2015-12-15 01:37:17','5','Coagra - Valparaiso',2,'Changed fecha_fin.',18,2),(84,'2015-12-15 01:37:29','4','copeval - O\'Higgins',2,'Changed fecha_fin.',18,2),(85,'2015-12-15 01:37:41','3','test alex - Metropolitana',2,'Changed fecha_fin.',18,2),(86,'2015-12-15 01:37:52','2','Oferta test 2 - Metropolitana',2,'Changed fecha_fin.',18,2),(87,'2015-12-15 01:38:00','1','Oferta test - Metropolitana',2,'Changed fecha_fin.',18,2),(88,'2015-12-22 21:34:28','1','Oferta test - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(89,'2016-01-12 03:22:51','5','Coagra - Valparaiso',3,'',18,1),(90,'2016-01-12 03:22:51','4','copeval - O\'Higgins',3,'',18,1),(91,'2016-01-12 03:22:51','3','test alex - Metropolitana',3,'',18,1),(92,'2016-01-12 03:22:51','2','Oferta test 2 - Metropolitana',3,'',18,1),(93,'2016-01-12 03:22:51','1','Oferta test - Metropolitana',3,'',18,1),(94,'2016-01-13 20:45:10','1','Agrocentro - Buin',1,'',22,2),(95,'2016-01-13 20:46:05','6','Agrocentro - Metropolitana',1,'',18,2),(96,'2016-01-13 20:46:58','7','Agrocentro - Roundap',1,'',19,2),(97,'2016-01-13 20:51:20','1','Agrocentro - Agrocentro',2,'Changed nombre.',22,2),(98,'2016-01-13 20:52:26','6','Agrocentro - Metropolitana',2,'Changed texto_despacho, direccion_sucursal and telefono_sucursal.',18,2),(99,'2016-01-13 20:53:52','6','Agrocentro - Metropolitana',2,'Changed texto_lateral_empresa_oferente and telefono_sucursal.',18,2),(100,'2016-01-13 20:55:44','6','Agrocentro - Metropolitana',2,'Changed nombre_sucursal.',18,2),(101,'2016-01-13 20:55:57','1','Agrocentro - Agrocentro',2,'Changed telefono.',22,2),(102,'2016-01-15 16:24:51','3','Roundap',2,'Changed archivo_pdf.',17,2),(103,'2016-01-15 16:27:01','3','Roundap',2,'Changed archivo_pdf.',17,2),(104,'2016-01-15 17:30:50','3','Roundap',2,'Changed foto_producto.',17,1),(105,'2016-01-15 17:30:57','3','Roundap',2,'Changed archivo_pdf.',17,1),(106,'2016-01-15 17:31:27','3','Roundap',2,'Changed archivo_pdf.',17,1),(107,'2016-01-15 17:39:57','3','Roundap',2,'Changed archivo_pdf.',17,1),(108,'2016-01-15 17:42:54','3','Roundap',2,'Changed archivo_pdf.',17,1),(109,'2016-01-15 17:47:53','3','Roundap',2,'Changed archivo_pdf.',17,1),(110,'2016-01-15 17:48:02','3','Roundap',2,'Changed foto_producto.',17,1),(111,'2016-01-15 17:51:10','3','Roundap',2,'Changed archivo_pdf.',17,1),(112,'2016-01-15 17:58:54','3','Roundap',2,'Changed archivo_pdf.',17,1),(113,'2016-01-15 18:05:31','3','Roundap',2,'Changed archivo_pdf and foto_producto.',17,1),(114,'2016-01-15 18:14:26','3','Roundap',2,'Changed archivo_pdf.',17,2),(115,'2016-01-15 18:22:57','1','Agrocentro - Agrocentro',2,'No fields changed.',22,2),(116,'2016-01-15 18:28:18','1','Agrocentro - Agrocentro',2,'Changed telefono.',22,2),(117,'2016-01-19 15:21:16','3','Roundap',2,'Changed foto_producto.',17,2),(118,'2016-01-19 15:24:21','5','Agrocentro',2,'Changed foto_empresa.',11,2),(119,'2016-01-19 15:25:44','5','Agrocentro',2,'Changed foto_empresa.',11,2),(120,'2016-01-19 15:26:41','5','Agrocentro',2,'Changed foto_empresa.',11,2),(121,'2016-01-19 15:26:46','5','Agrocentro',2,'No fields changed.',11,2),(122,'2016-01-19 15:28:39','5','Agrocentro',2,'Changed foto_empresa.',11,2),(123,'2016-01-19 15:29:05','5','Agrocentro',2,'Changed foto_empresa.',11,2),(124,'2016-01-19 15:33:24','3','Roundap',2,'Changed foto_producto.',17,2),(125,'2016-01-19 15:33:33','3','Roundap',2,'Changed foto_producto.',17,2),(126,'2016-01-19 15:34:19','5','Agrocentro',2,'Changed foto_empresa.',11,2),(127,'2016-01-19 15:34:30','5','Agrocentro',2,'Changed foto_empresa.',11,2),(128,'2016-01-19 15:35:13','5','Agrocentro',2,'Changed foto_empresa.',11,2),(129,'2016-01-19 15:35:21','5','Agrocentro',2,'Changed foto_empresa.',11,2),(130,'2016-01-19 15:35:46','5','Agrocentro',2,'Changed foto_empresa.',11,2),(131,'2016-01-19 15:35:52','5','Agrocentro',2,'Changed foto_empresa.',11,2),(132,'2016-01-20 21:02:41','3','Roundap',2,'Changed foto_producto.',17,2),(133,'2016-01-20 21:03:08','3','Roundap',2,'Changed foto_producto.',17,2),(134,'2016-01-20 21:04:22','5','Agrocentro',2,'Changed foto_empresa.',11,2),(135,'2016-01-20 21:04:41','5','Agrocentro',2,'Changed foto_empresa.',11,2),(136,'2016-01-20 21:10:04','5','Agrocentro',2,'Changed foto_empresa.',11,2),(137,'2016-01-20 21:10:18','5','Agrocentro',2,'Changed foto_empresa.',11,2),(138,'2016-01-20 21:10:45','3','Roundap',2,'Changed foto_producto.',17,2),(139,'2016-01-20 21:10:51','3','Roundap',2,'Changed foto_producto.',17,2),(140,'2016-01-20 21:39:57','8','Agrocentro - Urea granulada',1,'',19,2),(141,'2016-01-20 21:41:31','5','Urea granulada',2,'Changed archivo_pdf.',17,2),(142,'2016-01-20 21:43:13','5','Urea granulada',2,'Changed archivo_pdf and foto_producto.',17,2),(143,'2016-01-20 21:54:03','5','Urea granulada',2,'Changed foto_producto.',17,2),(144,'2016-01-20 21:54:19','5','Urea granulada',2,'Changed foto_producto.',17,2),(145,'2016-01-22 18:45:35','5','Agrocentro',2,'Changed email_vendedor.',11,2),(146,'2016-01-22 18:52:28','5','Agrocentro',2,'Changed email_vendedor.',11,2),(147,'2016-01-24 22:36:05','4','Karate Zeon',2,'Changed foto_producto.',17,2),(148,'2016-01-24 22:36:45','9','Agrocentro - Karate Zeon',1,'',19,2),(149,'2016-01-24 22:37:33','3','Roundap',2,'Changed foto_producto.',17,2),(150,'2016-01-24 22:37:57','3','Roundap',2,'Changed foto_producto.',17,2),(151,'2016-01-24 22:40:55','6','Agrocentro - Metropolitana',2,'Changed texto_despacho.',18,2),(152,'2016-01-24 22:47:19','5','Agrocentro',2,'Changed foto_empresa.',11,2),(153,'2016-01-24 22:47:52','5','Agrocentro',2,'Changed foto_empresa.',11,2),(154,'2016-01-24 22:49:30','5','Agrocentro',2,'Changed email_vendedor.',11,2),(155,'2016-01-24 22:49:36','5','Agrocentro',2,'No fields changed.',11,2),(156,'2016-01-24 22:51:02','1','Agrocentro - Agrocentro',2,'No fields changed.',22,2),(157,'2016-01-24 22:51:23','5','Agrocentro',2,'Changed foto_empresa.',11,2),(158,'2016-01-24 22:51:57','5','Agrocentro',2,'Changed foto_empresa.',11,2),(159,'2016-01-24 22:52:02','5','Agrocentro',2,'No fields changed.',11,2),(160,'2016-01-24 22:58:30','5','Agrocentro',2,'Changed foto_empresa.',11,2),(161,'2016-01-24 22:58:48','5','Agrocentro',2,'Changed foto_empresa.',11,2),(162,'2016-01-24 22:59:20','5','Agrocentro',2,'Changed foto_empresa.',11,2),(163,'2016-01-24 22:59:39','5','Agrocentro',2,'Changed foto_empresa.',11,2),(164,'2016-01-24 23:03:51','1','Agrocentro - Agrocentro',2,'Changed telefono.',22,2),(165,'2016-01-24 23:12:36','3','Coagra',2,'Changed rut, region, provincia, comuna and foto_empresa.',11,2),(166,'2016-01-24 23:16:13','3','Coagra',2,'Changed foto_empresa and email_vendedor.',11,2),(167,'2016-01-24 23:16:18','3','Coagra',2,'No fields changed.',11,2),(168,'2016-01-24 23:17:30','2','Coagra - Coagra',1,'',22,2),(169,'2016-01-24 23:19:13','8','Bravo 720',2,'Changed foto_producto.',17,2),(170,'2016-01-24 23:21:33','7','Coagra - Metropolitana',1,'',18,2),(171,'2016-01-24 23:22:57','2','Coagra - Coagra',2,'Changed latitud and longitud.',22,2),(172,'2016-01-24 23:25:38','10','Coagra - Bravo 720',1,'',19,2),(173,'2016-01-24 23:26:20','8','Bravo 720',2,'Changed archivo_pdf.',17,2),(174,'2016-01-24 23:26:58','8','Bravo 720',2,'Changed archivo_pdf.',17,2),(175,'2016-01-24 23:28:59','11','Coagra - Roundap',1,'',19,2),(176,'2016-01-24 23:33:25','9','Semilla Maiz P1780',2,'Changed nombre and archivo_pdf.',17,2),(177,'2016-01-24 23:36:44','9','Semilla Maiz P1780',2,'Changed archivo_pdf and foto_producto.',17,2),(178,'2016-01-24 23:39:18','12','Coagra - Semilla Maiz P1780',1,'',19,2),(179,'2016-01-24 23:42:35','4','CALS',2,'Changed razon_social, region, provincia, comuna and foto_empresa.',11,2),(180,'2016-01-24 23:50:36','4','CALS',2,'Changed foto_empresa and email_vendedor.',11,2),(181,'2016-01-24 23:51:34','3','CALS - CALS',1,'',22,2),(182,'2016-01-24 23:54:04','8','CALS - Metropolitana',1,'',18,2),(183,'2016-01-24 23:57:56','10','Sulfato de Potasio',1,'',17,2),(184,'2016-01-24 23:59:11','6','Tractor 6125D John Deere',2,'Changed foto_producto.',17,2),(185,'2016-01-25 00:02:36','13','CALS - Sulfato de Potasio',1,'',19,2),(186,'2016-01-25 00:07:10','14','CALS - Tractor 6125D John Deere',1,'',19,2),(187,'2016-01-25 00:07:43','15','CALS - Roundap',1,'',19,2),(188,'2016-01-27 20:41:37','9','Agr. Padre Hurtado',1,'',11,2),(189,'2016-01-27 21:00:24','4','Agr. Padre Hurtado - Agricola Padre Hurtado',1,'',22,2),(190,'2016-01-27 21:00:31','4','Agr. Padre Hurtado - Agricola Padre Hurtado',2,'Changed longitud.',22,2),(191,'2016-01-27 21:05:58','9','Agrícola Padre Hurtado - Metropolitana',1,'',18,2),(192,'2016-01-27 21:15:16','9','Padre Hurtado - Metropolitana',2,'Changed nombre.',18,2),(193,'2016-01-27 21:15:32','4','Agr. Padre Hurtado - Padre Hurtado',2,'Changed nombre.',22,2),(194,'2016-01-27 21:15:56','9','Agr. Padre Hurtado - Metropolitana',2,'Changed nombre and fecha_fin.',18,2),(195,'2016-01-27 21:16:05','8','CALS - Metropolitana',2,'Changed fecha_fin.',18,2),(196,'2016-01-27 21:16:17','7','Coagra - Metropolitana',2,'Changed fecha_fin.',18,2),(197,'2016-01-27 21:16:36','6','Agrocentro - Metropolitana',2,'Changed fecha_fin.',18,2),(198,'2016-01-27 21:23:22','11','Sulfato de Magnesio',1,'',17,2),(199,'2016-01-27 21:23:51','11','Sulfato de Magnesio',2,'Changed archivo_pdf.',17,2),(200,'2016-01-27 21:24:21','11','Sulfato de Magnesio',2,'Changed archivo_pdf.',17,2),(201,'2016-01-27 21:26:02','12','Dursban 48',1,'',17,2),(202,'2016-01-27 21:27:35','16','Agr. Padre Hurtado - Sulfato de Magnesio',1,'',19,2),(203,'2016-01-27 21:28:53','17','Agr. Padre Hurtado - Dursban 48',1,'',19,2),(204,'2016-01-27 21:29:42','18','Agr. Padre Hurtado - Roundap',1,'',19,2),(205,'2016-01-27 21:31:02','4','Agr. Padre Hurtado -  Agr. Padre Hurtado',2,'Changed nombre.',22,2),(206,'2016-01-27 21:31:53','12','Dursban 48',2,'Changed foto_producto.',17,2),(207,'2016-01-27 21:34:42','12','Dursban 48',2,'Changed foto_producto.',17,2),(208,'2016-01-27 21:36:03','14','CALS - Tractor 6125D John Deere',3,'',19,2),(209,'2016-01-27 21:39:05','13','Rooting',1,'',17,2),(210,'2016-01-27 21:44:02','19','Agr. Padre Hurtado - Rooting',1,'',19,2),(211,'2016-01-27 21:44:36','19','CALS - Rooting',2,'Changed oferta.',19,2),(212,'2016-01-27 22:01:04','9','Agr. Padre Hurtado',2,'No fields changed.',11,2),(213,'2016-01-27 22:01:18','4','Agr. Padre Hurtado -  Agr. Padre Hurtado',2,'No fields changed.',22,2),(214,'2016-01-28 04:27:47','5','sebastian',3,'',12,2),(215,'2016-01-28 04:27:47','3','salvo',3,'',12,2),(216,'2016-01-28 04:28:11','8','sebastian ',3,'',12,2),(217,'2016-01-28 04:28:11','7','alexis salvo',3,'',12,2),(218,'2016-01-28 04:28:11','2','alex',3,'',12,2),(219,'2016-01-28 04:29:05','11','sebastian navarro ',3,'',11,2),(220,'2016-01-28 04:29:05','10','Alexis Salvo',3,'',11,2),(221,'2016-01-28 04:29:05','7','juan valdes',3,'',11,2),(222,'2016-01-28 04:29:05','6','Inversiones Ltda.',3,'',11,2),(223,'2016-01-28 04:29:29','2','Tattersal',3,'',11,2),(224,'2016-02-29 21:03:43','6','Agrocentro - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(225,'2016-02-29 21:03:55','7','Coagra - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(226,'2016-02-29 21:04:16','8','CALS - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(227,'2016-02-29 21:04:38','9','Agr. Padre Hurtado - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(228,'2016-03-03 19:29:01','9','Agr. Padre Hurtado - Metropolitana',2,'Changed fecha_fin.',18,2),(229,'2016-03-03 19:29:07','7','Coagra - Metropolitana',2,'Changed fecha_fin.',18,2),(230,'2016-03-03 19:29:13','8','CALS - Metropolitana',2,'Changed fecha_fin.',18,2),(231,'2016-03-03 19:29:20','9','Agr. Padre Hurtado - Metropolitana',2,'Changed fecha_fin.',18,2),(232,'2016-03-03 19:29:28','6','Agrocentro - Metropolitana',2,'Changed fecha_fin.',18,2),(233,'2016-03-07 21:24:04','6','Agrocentro - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(234,'2016-03-08 22:32:56','8','Vendedor - Metropolitana',2,'Changed nombre.',18,2),(235,'2016-03-08 22:33:30','3','CALS - Vendedor',2,'Changed nombre.',22,2),(236,'2016-03-08 22:33:33','8','Vendedor - Metropolitana',2,'No fields changed.',18,2),(237,'2016-03-09 12:33:24','3','CALS - CALS',2,'Changed nombre.',22,2),(238,'2016-03-09 12:33:42','8','Vendedor - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(239,'2016-03-09 12:33:59','9','Agr. Padre Hurtado - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(240,'2016-03-09 12:34:13','7','Coagra - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(241,'2016-03-09 12:34:30','6','Agrocentro - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(242,'2016-03-09 12:34:53','8','CALS - Metropolitana',2,'Changed nombre.',18,2),(243,'2016-03-22 14:33:45','9','Agr. Padre Hurtado - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(244,'2016-03-22 14:35:15','8','CALS - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(245,'2016-03-22 14:35:38','7','Coagra - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(246,'2016-03-22 14:35:55','6','Agrocentro - Metropolitana',2,'Changed texto_despacho, fecha_inicio and fecha_fin.',18,2),(247,'2016-03-22 14:36:16','6','Agrocentro - Metropolitana',2,'Changed fecha_inicio and fecha_fin.',18,2),(248,'2016-03-22 21:00:34','14','Región Metropolitana',2,'Changed nombre.',8,3),(249,'2016-03-27 22:00:44','9','Agr. Padre Hurtado',2,'Changed foto_empresa.',11,2),(250,'2016-03-27 22:03:11','9','Agr. Padre Hurtado',2,'Changed foto_empresa.',11,2),(251,'2016-03-27 22:05:52','9','Agr. Padre Hurtado',2,'Changed foto_empresa.',11,2),(252,'2016-03-29 01:17:23','13','asdasda',2,'Changed foto_empresa.',11,3),(253,'2016-03-29 01:18:47','9','Agr. Padre Hurtado',2,'Changed foto_empresa.',11,3),(254,'2016-03-29 01:48:28','14','prueba',1,'Added.',17,3),(255,'2016-03-29 01:58:36','13','asdasda',2,'Changed foto_empresa.',11,3),(256,'2016-03-29 02:08:50','13','asdasda',2,'Changed foto_empresa.',11,3),(257,'2016-03-29 02:29:27','13','asdasda',2,'Changed foto_empresa.',11,3),(258,'2016-03-29 02:35:19','13','asdasda',2,'Changed foto_empresa.',11,3),(259,'2016-03-29 02:37:51','13','asdasda',2,'Changed foto_empresa.',11,3),(260,'2016-03-29 02:38:27','9','Agr. Padre Hurtado',2,'Changed foto_empresa.',11,3),(261,'2016-03-29 02:43:16','13','asdasda',2,'Changed foto_empresa.',11,3),(262,'2016-03-29 03:36:59','13','asdasda',2,'Changed foto_empresa.',11,3),(263,'2016-03-29 03:37:16','9','Agr. Padre Hurtado',2,'Changed foto_empresa.',11,3),(264,'2016-03-31 15:03:59','9','Agr. Padre Hurtado',2,'Changed fecha_inicio and fecha_fin.',18,2),(265,'2016-03-31 15:04:20','8','CALS',2,'Changed fecha_inicio and fecha_fin.',18,2),(266,'2016-03-31 15:04:55','7','Coagra',2,'Changed fecha_inicio and fecha_fin.',18,2),(267,'2016-03-31 15:05:57','6','Agrocentro',2,'Changed fecha_inicio and fecha_fin.',18,2),(268,'2016-03-31 16:24:49','4','CALS',2,'Changed foto_empresa.',11,2),(269,'2016-03-31 16:25:27','4','CALS',2,'Changed foto_empresa.',11,2),(270,'2016-03-31 16:25:30','3','CALS - CALS',2,'No fields changed.',22,2),(271,'2016-04-07 01:04:27','9','Agr. Padre Hurtado',2,'Changed fecha_inicio and fecha_fin.',18,2),(272,'2016-04-07 01:04:45','8','CALS',2,'Changed fecha_inicio and fecha_fin.',18,2),(273,'2016-04-07 01:05:03','7','Coagra',2,'Changed fecha_inicio and fecha_fin.',18,2),(274,'2016-04-07 01:05:12','8','CALS',2,'Changed count.',18,2),(275,'2016-04-07 01:05:24','6','Agrocentro',2,'Changed fecha_inicio and fecha_fin.',18,2),(276,'2016-04-08 01:08:45','4','KALS',2,'Changed razon_social.',11,2),(277,'2016-04-08 01:08:48','3','KALS - KALS',2,'Changed nombre.',22,2),(278,'2016-04-08 01:09:04','8','CALS',2,'Changed fecha_fin.',18,2),(279,'2016-04-08 01:09:43','9','Agr. Padre Hurtado',2,'Changed fecha_fin and count.',18,2),(280,'2016-04-08 01:10:18','3','Koagra',2,'Changed razon_social.',11,2),(281,'2016-04-08 01:10:19','2','Koagra - Koagra',2,'Changed nombre.',22,2),(282,'2016-04-08 01:10:27','7','Koagra',2,'Changed nombre and fecha_fin.',18,2),(283,'2016-04-08 01:11:03','5','Agrozentro',2,'Changed razon_social.',11,2),(284,'2016-04-08 01:11:08','1','Agrozentro - Agrozentro',2,'Changed nombre.',22,2),(285,'2016-04-08 01:11:24','6','Agrozentro',2,'Changed nombre, fecha_fin and count.',18,2),(286,'2016-04-08 01:11:38','8','KALS',2,'Changed nombre.',18,2),(287,'2016-04-11 20:28:25','1','Reporte del clima',1,'Added.',23,3),(288,'2016-04-11 20:32:47','1','Reporte del clima',2,'No fields changed.',23,3),(289,'2016-04-11 20:33:02','1','Reporte del clima',2,'Changed direccion.',23,3),(290,'2016-04-12 18:40:52','9','Agr. Padre Hurtado',2,'Changed fecha_inicio, fecha_fin and count.',18,2),(291,'2016-04-12 18:41:16','8','KALS',2,'Changed fecha_inicio, fecha_fin and count.',18,2),(292,'2016-04-12 18:41:45','7','Koagra',2,'Changed fecha_inicio and fecha_fin.',18,2),(293,'2016-04-12 18:42:08','6','Agrozentro',2,'Changed fecha_inicio, fecha_fin and count.',18,2),(294,'2016-04-14 01:11:18','1','Datos Agroclimáticos 13/04',2,'Changed nombre, direccion, latitud and longitud.',23,2),(295,'2016-04-14 01:12:22','1','Datos Agroclimáticos 13/04',2,'Changed latitud and longitud.',23,2),(296,'2016-04-14 01:22:49','2','Datos Agroclimáticos 13/04',1,'Added.',23,2),(297,'2016-04-14 01:25:20','1','Datos Agroclimáticos Huelquen (13/04)',2,'Changed nombre, latitud and longitud.',23,2),(298,'2016-04-14 01:25:49','2','Datos Agroclimáticos Talagante (13/04)',2,'Changed nombre.',23,2),(299,'2016-04-14 01:26:53','1','Datos Agroclimáticos Huelquen (13/04)',2,'Changed direccion.',23,2),(300,'2016-04-14 01:26:59','2','Datos Agroclimáticos Talagante (13/04)',2,'No fields changed.',23,2);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(20,'oferta','asociacionofertaproductocotizacion'),(21,'oferta','cotizacionproductoroferta'),(18,'oferta','oferta'),(19,'oferta','productoenoferta'),(10,'registro','comuna'),(11,'registro','empresa'),(14,'registro','especie'),(17,'registro','producto'),(12,'registro','productor'),(15,'registro','productorespecie'),(9,'registro','provincia'),(8,'registro','region'),(22,'registro','sucursal'),(13,'registro','tipoespecie'),(16,'registro','tipoproductos'),(23,'registro','tips'),(7,'registro','usuario'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2015-11-12 04:17:49'),(2,'auth','0001_initial','2015-11-12 04:17:49'),(3,'admin','0001_initial','2015-11-12 04:17:49'),(4,'contenttypes','0002_remove_content_type_name','2015-11-12 04:17:49'),(5,'auth','0002_alter_permission_name_max_length','2015-11-12 04:17:49'),(6,'auth','0003_alter_user_email_max_length','2015-11-12 04:17:49'),(7,'auth','0004_alter_user_username_opts','2015-11-12 04:17:50'),(8,'auth','0005_alter_user_last_login_null','2015-11-12 04:17:50'),(9,'auth','0006_require_contenttypes_0002','2015-11-12 04:17:50'),(10,'registro','0001_initial','2015-11-12 04:17:50'),(11,'oferta','0001_initial','2015-11-12 04:17:50'),(12,'oferta','0002_auto_20151112_0253','2015-11-12 04:17:50'),(13,'sessions','0001_initial','2015-11-12 04:17:51'),(14,'oferta','0003_auto_20151202_1708','2015-12-03 22:23:30'),(15,'registro','0002_auto_20151202_1708','2015-12-03 22:23:30'),(16,'registro','0003_sucursal','2016-01-12 03:22:11'),(17,'oferta','0004_auto_20160110_2224','2016-01-12 03:25:12'),(18,'registro','0004_auto_20160110_2235','2016-01-12 03:25:17'),(19,'oferta','0005_cotizacionproductoroferta_fecha','2016-01-20 03:13:06');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0bvnk6izrqjhuq01z1rfro8j6my9s7pe','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-08 02:46:56'),('0fox5rao4uva1hy581xljaxwcg2a77ov','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-10 21:45:20'),('15xh7pys77h0rhuvdhe8rf21brld9zp3','NDdmMDQ3YjYzZGU5OTM5MGRmYTFjMmJkZmJkNzljNTMxNWU4YTJlNjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWEyMjk5MjEwODgxZDc1YmU2NzRhMTcyM2FkMmI5ZWM3NTFjOWZiZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwicHJvZHVjdG9yX2lkIjoiZ3J1bmdlcm8ifQ==','2015-12-12 02:24:59'),('25cinfeoxk0ltoyfgv6wyjj9u6ynmh8v','NzkzNmFkZGUwMmVkYTQ1NzAzNGE5MDhkOTZmOTY0YWVkNWE2OGJjMTp7InByb2R1Y3Rvcl9pZCI6InNhbHZvIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWEyMjk5MjEwODgxZDc1YmU2NzRhMTcyM2FkMmI5ZWM3NTFjOWZiZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2015-12-15 04:05:52'),('2dxzzod2dj5nwcqo8qj6loa91qzf7x5u','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-22 18:27:54'),('3yzt4csl6p9e5pbszorhhr877n2vsv16','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-02-05 02:01:32'),('40loplwc5g5nwvz4fzfk32ri4txgf6wf','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-02-08 14:27:52'),('44x6tdd0shzk0mtx156prcywfogva2wf','Zjk1ODNiMTgzMjMzOWI1N2QzMGQxMWRjODIyYWIyM2FiOGFkODQwYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjFjM2MzMGE2OWMzNTM2MTRhMTBhZWQ1OWE1ZDUwMGQ5ZWNmOTFmNiIsIl9hdXRoX3VzZXJfaWQiOiIzIiwicHJvZHVjdG9yX2lkIjoiYWxleGlzIn0=','2016-04-12 01:16:37'),('4qwoy6ap0l7438oxwc38t8dt0szf27yd','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-02 23:51:45'),('4rzpjd3w5czmd7a2s0xsrd1hrbmmvo94','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-02-11 04:38:52'),('4u6dtd9quaviyzw9e7yt9ump1o7wiuza','ZTRhYzcyZGM3YjZmMTBlODUyMDdhNWZlMGIxMzUxMWRmZWZiYzlmNzp7InByb2R1Y3Rvcl9pZCI6ImFsZXhpcyJ9','2016-05-04 18:04:39'),('5e2fed6dx1khij8i0p9ed077oymbx7vl','NjEwZDU3MmI0OWZmMDQzMDA2ZDIwN2QyNzdhOGU4ZWNkZWU0NGVhMzp7InByb2R1Y3Rvcl9pZCI6ImdydW5nZXJvIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWEyMjk5MjEwODgxZDc1YmU2NzRhMTcyM2FkMmI5ZWM3NTFjOWZiZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2015-12-17 22:25:18'),('5nvmtfoof1vcg79m3oz5uzkwfebkqsec','NGQwNjdkYTZkYTc2NjVhN2VkZWUzMjc1NzkzZTYxMzViYjdiOTgyZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlhMjI5OTIxMDg4MWQ3NWJlNjc0YTE3MjNhZDJiOWVjNzUxYzlmYmYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2015-11-29 04:12:42'),('5ozr2web1mopoggqo3yqz0tu8rvnc9xg','YWQzMTZiNTAxODRkMjA4NGE1MmViNTIzOTU5OTk4YTJiMjcxMzM0ZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFkNTdkNDU3OGRlYjkxYTFlNjA3MmQyNGNlZDA3MzI3MjQwZDdmYjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=','2016-05-05 02:23:03'),('65giu20a9xrce0thz82rd8ki4j9asz3r','MDViZmNiOWRkNDJkMDkzNzUwMzZkYTI1Zjk5MWMwNzk0ZGMxZTBhZDp7InByb2R1Y3Rvcl9pZCI6ImdydW5nZXJvIn0=','2016-02-03 03:18:39'),('6e3ayki5irca17vlpicuu4cr5wigrb7e','NjEwZDU3MmI0OWZmMDQzMDA2ZDIwN2QyNzdhOGU4ZWNkZWU0NGVhMzp7InByb2R1Y3Rvcl9pZCI6ImdydW5nZXJvIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWEyMjk5MjEwODgxZDc1YmU2NzRhMTcyM2FkMmI5ZWM3NTFjOWZiZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2015-12-13 21:34:34'),('6g0o4bnu7i5burb16lisw8y1vqw4lpdl','MWY1NWY2MmFjYzUwNmNiYTcyNDRjNjY5MTM2ZjM2ZTIyNjE2ZGJlNzp7InByb2R1Y3Rvcl9pZCI6ImFsZXhpcyIsIl9hdXRoX3VzZXJfaGFzaCI6ImY4MjZlZjI4ZmJlZjljMzRhOGQzMDNmMDk0MjQ3NjBiOGIyZmIxNjciLCJfYXV0aF91c2VyX2lkIjoiMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-12-15 03:22:28'),('7dkb3g84hnssxmagkrndicmvnz6rn2i6','MDViZmNiOWRkNDJkMDkzNzUwMzZkYTI1Zjk5MWMwNzk0ZGMxZTBhZDp7InByb2R1Y3Rvcl9pZCI6ImdydW5nZXJvIn0=','2016-02-04 17:55:08'),('7s87ndvdxuht1ao5i501r82f08lvd1l4','MDNmZmJlZTdhODhiZTYyMjVmYzFiM2VhZDdiOWJhYjllOTUzNGI4Njp7InByb2R1Y3Rvcl9pZCI6IkFsZXhpcyJ9','2016-04-11 01:00:37'),('7wsj1dbw2imwn4sjytu60v9eei7o3413','NjEwZDU3MmI0OWZmMDQzMDA2ZDIwN2QyNzdhOGU4ZWNkZWU0NGVhMzp7InByb2R1Y3Rvcl9pZCI6ImdydW5nZXJvIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWEyMjk5MjEwODgxZDc1YmU2NzRhMTcyM2FkMmI5ZWM3NTFjOWZiZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2015-12-16 16:32:01'),('807uyzgxl5gif9miyn6zr667ue0adh32','NjEwZDU3MmI0OWZmMDQzMDA2ZDIwN2QyNzdhOGU4ZWNkZWU0NGVhMzp7InByb2R1Y3Rvcl9pZCI6ImdydW5nZXJvIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWEyMjk5MjEwODgxZDc1YmU2NzRhMTcyM2FkMmI5ZWM3NTFjOWZiZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2015-11-26 04:24:02'),('84vsttuxpivpmgsgmd7zct88dp3py2gl','ZTRhYzcyZGM3YjZmMTBlODUyMDdhNWZlMGIxMzUxMWRmZWZiYzlmNzp7InByb2R1Y3Rvcl9pZCI6ImFsZXhpcyJ9','2016-04-29 00:56:32'),('9gmkgo9eoax8b3xynaymf0nqohluq1rq','YWQzMTZiNTAxODRkMjA4NGE1MmViNTIzOTU5OTk4YTJiMjcxMzM0ZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFkNTdkNDU3OGRlYjkxYTFlNjA3MmQyNGNlZDA3MzI3MjQwZDdmYjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=','2016-05-02 21:17:34'),('9hr96pjzlnkbbmbrbnqwp1113sqmwdu0','MDViZmNiOWRkNDJkMDkzNzUwMzZkYTI1Zjk5MWMwNzk0ZGMxZTBhZDp7InByb2R1Y3Rvcl9pZCI6ImdydW5nZXJvIn0=','2016-02-05 10:26:20'),('9wyx8kwajr0bs0vl794g6obpwdgs5u5c','YWQzMTZiNTAxODRkMjA4NGE1MmViNTIzOTU5OTk4YTJiMjcxMzM0ZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFkNTdkNDU3OGRlYjkxYTFlNjA3MmQyNGNlZDA3MzI3MjQwZDdmYjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=','2016-05-05 04:02:53'),('aikqplkn4a7c3t0wv3ch681nxdjs40xe','ZWMxZDc1MzRmYjA2NmI5ZWY4MGM2Y2MyOTg5MDNiNDI3OTA4ZDNjNTp7InByb2R1Y3Rvcl9pZCI6ImFsZXgiLCJfYXV0aF91c2VyX2hhc2giOiJmODI2ZWYyOGZiZWY5YzM0YThkMzAzZjA5NDI0NzYwYjhiMmZiMTY3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMiJ9','2016-01-29 23:00:30'),('b85ox106mxsk0oprdwjm9xqdx3l875gs','ZmFjMzk3ZWIyYmM2OWFiZTZkN2UyYmExOWQzMDAxOGI3MzYyNjljZTp7InByb2R1Y3Rvcl9pZCI6ImFsZXgiLCJfYXV0aF91c2VyX2hhc2giOiJmODI2ZWYyOGZiZWY5YzM0YThkMzAzZjA5NDI0NzYwYjhiMmZiMTY3IiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2015-12-17 19:11:14'),('bdqbsanovrwld176uwyfmd8evikt8a34','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-02-04 21:57:18'),('dc6xtcj1xczv4l5pvhcf3qrcbjkzorff','ZTRhYzcyZGM3YjZmMTBlODUyMDdhNWZlMGIxMzUxMWRmZWZiYzlmNzp7InByb2R1Y3Rvcl9pZCI6ImFsZXhpcyJ9','2016-03-23 13:52:59'),('frwhn0qmfi8rzl7di5j82bdjf3gk0sp3','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-02-05 14:52:03'),('g3k1ymbqf8yxt9tbyvjjrg3p43b3mlj0','MDViZmNiOWRkNDJkMDkzNzUwMzZkYTI1Zjk5MWMwNzk0ZGMxZTBhZDp7InByb2R1Y3Rvcl9pZCI6ImdydW5nZXJvIn0=','2015-12-16 17:37:50'),('g5p33k3kaw7dzmossbvdodp87satteb1','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2015-12-02 02:30:26'),('gqa9gywjwkuxolstldnhjrqh5idw03jg','OWE2ZDczMjQxMDMyNjNkZGZjZDQyNmIwYWNkMzhmZmVkMTEyNTM3ODp7InByb2R1Y3Rvcl9pZCI6ImFsZXgifQ==','2016-02-05 19:21:34'),('hbnldhwjvg6thlnkdcqrilimhnd7ozz5','NGQwNjdkYTZkYTc2NjVhN2VkZWUzMjc1NzkzZTYxMzViYjdiOTgyZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlhMjI5OTIxMDg4MWQ3NWJlNjc0YTE3MjNhZDJiOWVjNzUxYzlmYmYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2016-01-26 03:22:36'),('hq2r0oky5bgokqyj66op5mli9s27ozrq','N2U1Yjc0ZmIwYTI3YWZlODZmYzczM2EyMzc4ZjljYmU2NzQ3ZDQ0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6ImY4MjZlZjI4ZmJlZjljMzRhOGQzMDNmMDk0MjQ3NjBiOGIyZmIxNjciLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=','2015-12-11 18:14:01'),('hu73ykvg6qss22eoudlkg1snk1wktoij','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-10 22:59:25'),('iz6v2v69d5xyia1duvptg7zzyctfrzor','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-14 12:25:09'),('j7bhm0zhpgch0gu5roz5dkh7zvwfd9sl','ZTRhYzcyZGM3YjZmMTBlODUyMDdhNWZlMGIxMzUxMWRmZWZiYzlmNzp7InByb2R1Y3Rvcl9pZCI6ImFsZXhpcyJ9','2016-04-17 18:26:58'),('jyk7up7mxsvwz1xfotd9fo6f3ta8ycnt','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2015-12-17 16:51:06'),('n1nt1e2h7vhnahhm3zrh9brs8mijk0bi','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-15 14:37:54'),('n52rzjibxuodksfx631nrsoo7wspxz9l','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-02-11 14:51:12'),('o26g78cmhmqkkcpcav1r7xw7hv5e98sc','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-02-03 02:10:09'),('ofykbmf881itsejrgjx3q3mx4j50ulfl','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-02-03 17:33:11'),('p432r9fbfjkfjuz5fzzvnkp5dyojkf9f','Zjk1ODNiMTgzMjMzOWI1N2QzMGQxMWRjODIyYWIyM2FiOGFkODQwYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjFjM2MzMGE2OWMzNTM2MTRhMTBhZWQ1OWE1ZDUwMGQ5ZWNmOTFmNiIsIl9hdXRoX3VzZXJfaWQiOiIzIiwicHJvZHVjdG9yX2lkIjoiYWxleGlzIn0=','2016-04-25 20:27:11'),('rbn9b0gk0vfa91w75idoddpiwknsu0sa','MDNmZmJlZTdhODhiZTYyMjVmYzFiM2VhZDdiOWJhYjllOTUzNGI4Njp7InByb2R1Y3Rvcl9pZCI6IkFsZXhpcyJ9','2016-04-05 20:25:07'),('shhypdg8yfgdqo6cnl56iha0120zdopg','ZTRhYzcyZGM3YjZmMTBlODUyMDdhNWZlMGIxMzUxMWRmZWZiYzlmNzp7InByb2R1Y3Rvcl9pZCI6ImFsZXhpcyJ9','2016-04-13 21:47:37'),('t24ejapw2mc7ubgmxks6gd1xvaxgfxl4','NGQwNjdkYTZkYTc2NjVhN2VkZWUzMjc1NzkzZTYxMzViYjdiOTgyZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlhMjI5OTIxMDg4MWQ3NWJlNjc0YTE3MjNhZDJiOWVjNzUxYzlmYmYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2016-02-11 03:31:59'),('tp0wcvq31jdrrozw83q5235k3iltup42','ZTRhYzcyZGM3YjZmMTBlODUyMDdhNWZlMGIxMzUxMWRmZWZiYzlmNzp7InByb2R1Y3Rvcl9pZCI6ImFsZXhpcyJ9','2016-04-25 13:43:37'),('u0ghhcy1ljr9ms0fgm3q6xim34voilxm','Zjk1ODNiMTgzMjMzOWI1N2QzMGQxMWRjODIyYWIyM2FiOGFkODQwYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjFjM2MzMGE2OWMzNTM2MTRhMTBhZWQ1OWE1ZDUwMGQ5ZWNmOTFmNiIsIl9hdXRoX3VzZXJfaWQiOiIzIiwicHJvZHVjdG9yX2lkIjoiYWxleGlzIn0=','2016-04-05 20:46:21'),('u8ialr42wwrtqhuq57jyu1powix3d7vj','NDdmMDQ3YjYzZGU5OTM5MGRmYTFjMmJkZmJkNzljNTMxNWU4YTJlNjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWEyMjk5MjEwODgxZDc1YmU2NzRhMTcyM2FkMmI5ZWM3NTFjOWZiZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwicHJvZHVjdG9yX2lkIjoiZ3J1bmdlcm8ifQ==','2015-12-14 19:41:31'),('uwh2b7pwl62johuwcj7vnyr9yck4vonh','OWE2ZDczMjQxMDMyNjNkZGZjZDQyNmIwYWNkMzhmZmVkMTEyNTM3ODp7InByb2R1Y3Rvcl9pZCI6ImFsZXgifQ==','2016-02-04 14:58:56'),('vfd8p39w3y1fnz9fncg4gk6n83t4lkjs','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2015-12-17 16:55:29'),('w9ps7oocm9tq6384kalyhtplig0dvkxb','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-19 00:26:06'),('we9o5e10ttvoccwuyrgsrpse2ck9uhpv','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-14 15:25:47'),('wiqk4itr5jotah8qd15vqoqlwbsog32s','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-14 16:33:25'),('xucegj4eqx34fsxh0sczpk9rql0ktglc','MDViZmNiOWRkNDJkMDkzNzUwMzZkYTI1Zjk5MWMwNzk0ZGMxZTBhZDp7InByb2R1Y3Rvcl9pZCI6ImdydW5nZXJvIn0=','2016-02-05 10:27:50'),('y32kpqjh0m70rfqurt9iziauyrson6pf','NjEwZDU3MmI0OWZmMDQzMDA2ZDIwN2QyNzdhOGU4ZWNkZWU0NGVhMzp7InByb2R1Y3Rvcl9pZCI6ImdydW5nZXJvIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWEyMjk5MjEwODgxZDc1YmU2NzRhMTcyM2FkMmI5ZWM3NTFjOWZiZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2016-01-29 17:24:18'),('z48ot3awyi5gsuyz3at28ui5x231s7me','MDNmZmJlZTdhODhiZTYyMjVmYzFiM2VhZDdiOWJhYjllOTUzNGI4Njp7InByb2R1Y3Rvcl9pZCI6IkFsZXhpcyJ9','2016-05-02 20:18:16'),('zixgabmz36ppf62cy75dwlgaj132bvd0','NzljNGNmMzg5MTgxNzgzNDFkNWYyNjNmNWIzODJhNTc5MmVhMTZhNDp7fQ==','2016-04-03 23:56:47'),('zxi56dgv5pgwmzn80cvxagnv2lgshhcn','ZWEzNWIxNDViN2U1M2EyOWQ1NjUyMGY3NjU1NTc5OTljOWI5NWMxNDp7InByb2R1Y3Rvcl9pZCI6ImFsZXggIn0=','2016-02-10 21:58:06');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oferta_asociacionofertaproductocotizacion`
--

DROP TABLE IF EXISTS `oferta_asociacionofertaproductocotizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oferta_asociacionofertaproductocotizacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad_cotizada` int(10) unsigned NOT NULL,
  `oferta_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `oferta_asociacionofertaproductocotizacion_ab6d9158` (`oferta_id`),
  KEY `oferta_asociacionofertaproductocotizacion_bb91903a` (`producto_id`),
  CONSTRAINT `oferta_asoc_producto_id_659935101975d80e_fk_registro_producto_id` FOREIGN KEY (`producto_id`) REFERENCES `registro_producto` (`id`),
  CONSTRAINT `oferta_asociacion_oferta_id_3024e32affabf411_fk_oferta_oferta_id` FOREIGN KEY (`oferta_id`) REFERENCES `oferta_oferta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oferta_asociacionofertaproductocotizacion`
--

LOCK TABLES `oferta_asociacionofertaproductocotizacion` WRITE;
/*!40000 ALTER TABLE `oferta_asociacionofertaproductocotizacion` DISABLE KEYS */;
INSERT INTO `oferta_asociacionofertaproductocotizacion` VALUES (1,3,6,3),(2,2,6,5),(3,4,6,3),(4,8,6,5),(5,12,6,3),(6,32,6,5),(7,3,6,3),(8,4,6,5),(9,1,6,3),(10,2,6,5),(11,22,6,3),(12,3,6,3),(13,6,6,5),(14,44,6,3),(15,5,6,3),(16,11,6,3),(17,3,6,3),(18,5,6,5),(19,3,6,3),(20,8,6,5),(21,4,6,3),(22,7,6,5),(23,1,6,3),(24,2,6,5),(25,1,6,3),(26,8,6,5),(27,1,6,5),(28,5,6,3),(29,5,6,5),(30,2,6,3),(31,7,6,5),(32,6,9,3),(33,5,7,3),(34,3,9,11),(35,39,7,3),(36,23,6,3),(37,8,8,3),(38,17,8,13),(39,2,6,3),(40,34,8,3),(41,11,8,13),(42,2,8,10),(43,45,8,3),(44,8,8,10),(45,12,8,3),(46,5,8,13),(47,55,7,3),(48,33,8,13),(49,2,8,10),(50,22,9,3),(51,1,8,10),(52,1,8,13),(53,2,7,3),(54,222,8,10),(55,2,8,13),(56,1,7,9),(57,7,8,13),(58,22,6,4),(59,90,7,9),(60,2,8,13),(61,22,8,13),(62,12,8,13),(63,12,8,13),(64,12,8,13),(65,12,8,13),(66,12,8,13),(67,12,8,13),(68,12,8,13),(69,12,8,13),(70,12,8,13),(71,12,8,13),(72,12,8,13),(73,12,8,13),(74,12,8,13),(75,12,8,13),(76,12,8,13),(77,12,8,13),(78,12,8,13),(79,12,8,13),(80,12,8,13),(81,12,9,3),(82,15,8,13),(83,12,8,13),(84,12,8,13),(85,12,8,13),(86,12,8,13),(87,12,8,13),(88,12,8,13),(89,12,8,13),(90,12,8,13),(91,12,8,13),(92,12,8,13),(93,2,9,12),(94,55,9,3),(95,44,6,5),(96,1,6,4),(97,309,9,3),(98,2,8,10),(99,55,8,3),(100,7,8,13),(101,77,6,5),(102,33,6,4),(103,22,8,13);
/*!40000 ALTER TABLE `oferta_asociacionofertaproductocotizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oferta_cotizacionproductoroferta`
--

DROP TABLE IF EXISTS `oferta_cotizacionproductoroferta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oferta_cotizacionproductoroferta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asociacion_cotizacion_productor_id` int(11) NOT NULL,
  `productor_id` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `D17cc8b076146a086cc9cdd7bfc8457f` (`asociacion_cotizacion_productor_id`),
  KEY `oferta_co_productor_id_48cfdbe84ada5780_fk_registro_productor_id` (`productor_id`),
  CONSTRAINT `D17cc8b076146a086cc9cdd7bfc8457f` FOREIGN KEY (`asociacion_cotizacion_productor_id`) REFERENCES `oferta_asociacionofertaproductocotizacion` (`id`),
  CONSTRAINT `oferta_co_productor_id_48cfdbe84ada5780_fk_registro_productor_id` FOREIGN KEY (`productor_id`) REFERENCES `registro_productor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oferta_cotizacionproductoroferta`
--

LOCK TABLES `oferta_cotizacionproductoroferta` WRITE;
/*!40000 ALTER TABLE `oferta_cotizacionproductoroferta` DISABLE KEYS */;
INSERT INTO `oferta_cotizacionproductoroferta` VALUES (1,1,1,'2016-01-20 22:18:12'),(2,2,1,'2016-01-20 22:18:12'),(3,3,1,'2016-01-20 22:20:43'),(4,4,1,'2016-01-20 22:20:43'),(5,5,1,'2016-01-20 22:26:44'),(6,6,1,'2016-01-20 22:26:44'),(7,7,1,'2016-01-20 22:31:12'),(8,8,1,'2016-01-20 22:31:12'),(9,9,1,'2016-01-21 15:24:29'),(10,10,1,'2016-01-21 15:24:29'),(12,12,1,'2016-01-21 17:55:45'),(13,13,1,'2016-01-21 17:55:45'),(14,14,1,'2016-01-21 21:48:03'),(15,15,1,'2016-01-22 02:01:19'),(17,17,1,'2016-01-22 03:07:33'),(18,18,1,'2016-01-22 03:07:33'),(19,19,1,'2016-01-22 10:26:53'),(20,20,1,'2016-01-22 10:26:53'),(21,21,1,'2016-01-22 10:28:30'),(22,22,1,'2016-01-22 10:28:30'),(27,27,1,'2016-01-22 19:36:48'),(35,35,9,'2016-01-28 04:32:31'),(36,36,9,'2016-01-28 14:47:43'),(37,37,9,'2016-02-29 21:08:57'),(38,38,9,'2016-02-29 21:08:57'),(39,39,9,'2016-03-07 21:27:13'),(40,40,9,'2016-03-17 19:56:19'),(41,41,9,'2016-03-17 19:56:19'),(42,42,9,'2016-03-23 22:16:00'),(43,43,9,'2016-03-23 22:16:00'),(44,44,9,'2016-03-27 21:43:00'),(45,45,9,'2016-03-27 21:43:00'),(46,46,9,'2016-03-27 21:44:22'),(47,47,9,'2016-03-27 21:44:58'),(48,48,9,'2016-03-27 21:52:32'),(49,49,9,'2016-03-27 21:55:03'),(50,50,9,'2016-03-27 22:07:24'),(51,51,9,'2016-03-27 22:54:12'),(52,52,9,'2016-03-28 00:30:28'),(53,53,9,'2016-03-30 18:13:28'),(54,54,9,'2016-03-30 18:15:11'),(55,55,9,'2016-03-30 22:37:37'),(56,56,9,'2016-03-31 12:24:35'),(57,57,9,'2016-04-01 14:37:05'),(58,58,9,'2016-04-03 18:27:32'),(59,59,9,'2016-04-04 14:44:04'),(60,60,9,'2016-04-04 16:17:37'),(61,61,9,'2016-04-04 16:21:31'),(62,62,9,'2016-04-04 17:19:53'),(63,63,9,'2016-04-04 17:26:40'),(64,64,9,'2016-04-04 17:27:47'),(65,65,9,'2016-04-04 17:36:31'),(66,66,9,'2016-04-04 17:43:17'),(67,67,9,'2016-04-04 17:51:04'),(68,68,9,'2016-04-04 17:52:42'),(69,69,9,'2016-04-04 18:01:01'),(70,70,9,'2016-04-04 18:02:17'),(71,71,9,'2016-04-04 18:04:33'),(72,72,9,'2016-04-04 18:07:07'),(73,73,9,'2016-04-04 18:09:40'),(74,74,9,'2016-04-04 18:11:03'),(75,75,9,'2016-04-04 18:12:06'),(76,76,9,'2016-04-04 18:15:56'),(77,77,9,'2016-04-04 18:17:25'),(78,78,9,'2016-04-04 18:19:06'),(79,79,9,'2016-04-04 18:22:00'),(80,80,9,'2016-04-04 18:22:26'),(81,81,9,'2016-04-04 18:23:47'),(82,82,9,'2016-04-04 18:29:06'),(83,83,9,'2016-04-04 18:32:41'),(84,84,9,'2016-04-04 18:39:17'),(85,85,9,'2016-04-04 18:41:42'),(86,86,9,'2016-04-04 18:49:45'),(87,87,9,'2016-04-04 18:50:32'),(88,88,9,'2016-04-04 18:52:44'),(89,89,9,'2016-04-04 18:56:19'),(90,90,9,'2016-04-04 18:57:28'),(91,91,9,'2016-04-04 19:06:42'),(92,92,9,'2016-04-04 19:11:08'),(93,93,9,'2016-04-04 19:23:04'),(94,94,9,'2016-04-04 19:23:04'),(95,95,9,'2016-04-05 00:24:50'),(96,96,9,'2016-04-05 00:24:50'),(97,97,9,'2016-04-05 19:51:30'),(98,98,9,'2016-04-08 01:16:00'),(99,99,9,'2016-04-08 01:16:00'),(100,100,9,'2016-04-08 01:16:00'),(101,101,9,'2016-04-11 22:41:35'),(102,102,9,'2016-04-11 22:41:35'),(103,103,9,'2016-04-14 01:00:15');
/*!40000 ALTER TABLE `oferta_cotizacionproductoroferta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oferta_oferta`
--

DROP TABLE IF EXISTS `oferta_oferta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oferta_oferta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `ahorro` int(10) unsigned NOT NULL,
  `texto_despacho` varchar(500) DEFAULT NULL,
  `texto_lateral_metodo_pago` varchar(500) DEFAULT NULL,
  `texto_lateral_metodo_entrega` varchar(500) DEFAULT NULL,
  `texto_lateral_empresa_oferente` varchar(500) DEFAULT NULL,
  `nombre_sucursal` varchar(500) DEFAULT NULL,
  `direccion_sucursal` varchar(500) DEFAULT NULL,
  `telefono_sucursal` varchar(500) DEFAULT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `activa` tinyint(1) NOT NULL,
  `region_id` int(11) NOT NULL,
  `sucursal_id` int(11) NOT NULL,
  `count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `oferta_oferta_region_id_6cc72feb9e3a70ac_fk_registro_region_id` (`region_id`),
  KEY `oferta_oferta_9d6d1627` (`sucursal_id`),
  CONSTRAINT `oferta_oferta_region_id_6cc72feb9e3a70ac_fk_registro_region_id` FOREIGN KEY (`region_id`) REFERENCES `registro_region` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oferta_oferta`
--

LOCK TABLES `oferta_oferta` WRITE;
/*!40000 ALTER TABLE `oferta_oferta` DISABLE KEYS */;
INSERT INTO `oferta_oferta` VALUES (6,'Agrozentro',20,'Entrega a domicilio sólo por compras mayores a U$350. Descuentos sobre precio de lista, consultar directamente precio final.','','','AGROCENTRO','Agrocentro','Av. Longuitudinal 190','9999999','2016-04-12 19:45:04','2016-04-18 00:00:00',1,14,1,26),(7,'Koagra',0,'Descuentos solo para pago en efectivo o documento a 30 días. Despacho a RM por compras sobre U$300','','','','','','','2016-04-12 19:44:46','2016-04-18 18:00:00',1,14,2,18),(8,'KALS',0,'Descuento valido para pagos en efectivo. Retiro en sucursal','','','','','','','2016-04-12 19:44:10','2016-04-18 06:00:00',1,14,3,25),(9,'Agr. Padre Hurtado',0,'Descuentos validos para compras superiores a U$ 300. Retiro en Sucursal. Pago sólo en efectivo.','','','','','','','2016-04-12 19:43:38','2016-04-18 12:00:00',1,14,4,21);
/*!40000 ALTER TABLE `oferta_oferta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oferta_productoenoferta`
--

DROP TABLE IF EXISTS `oferta_productoenoferta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oferta_productoenoferta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `porcentaje_descuento` int(11) NOT NULL,
  `envoltorio` varchar(200) NOT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `oferta_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `oferta_productoen_oferta_id_7fd1260278400d1c_fk_oferta_oferta_id` (`oferta_id`),
  KEY `oferta_prod_producto_id_345f593236aa201b_fk_registro_producto_id` (`producto_id`),
  CONSTRAINT `oferta_prod_producto_id_345f593236aa201b_fk_registro_producto_id` FOREIGN KEY (`producto_id`) REFERENCES `registro_producto` (`id`),
  CONSTRAINT `oferta_productoen_oferta_id_7fd1260278400d1c_fk_oferta_oferta_id` FOREIGN KEY (`oferta_id`) REFERENCES `oferta_oferta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oferta_productoenoferta`
--

LOCK TABLES `oferta_productoenoferta` WRITE;
/*!40000 ALTER TABLE `oferta_productoenoferta` DISABLE KEYS */;
INSERT INTO `oferta_productoenoferta` VALUES (7,25,'10 Lt','Herbicida de Contacto y Residual',6,3),(8,13,'50 Kg','Fuente Nitrogenada. Uso en Fertirriego',6,5),(9,10,'1 Lt','Insecticida amplio espectro|Control de chanchito, conchuela, pulgones',6,4),(10,25,'1 Lt','Fungicida de contacto, amplio espectro de acción en enfermedades',7,8),(11,40,'20 Lt','Herbicida de Contacto y Residual',7,3),(12,14,'10 Kg','Semilla de Maíz. Alto de rendimiento y precocidad.',7,9),(13,30,'50 Kg','Fuente de Potasio. Recomendado para crecimiento de fruta',8,10),(15,32,'20 Lt','Herbicida de Contacto y Residual',8,3),(16,10,'25 Kg','Fuente de Magnesio. Recomendado para el periodo de postcosecha',9,11),(17,14,'1 Lt','Insecticida amplio espectro y gran poder residual ',9,12),(18,19,'10 Lt','Herbicida no selectivo.',9,3),(19,20,'1 Lt','Enraizante, ideal para el periodo de postcosecha',8,13);
/*!40000 ALTER TABLE `oferta_productoenoferta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_comuna`
--

DROP TABLE IF EXISTS `registro_comuna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_comuna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `provincia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_comuna_54bf7e76` (`provincia_id`),
  CONSTRAINT `registro_comuna_provincia_id_33406e7c_fk_registro_provincia_id` FOREIGN KEY (`provincia_id`) REFERENCES `registro_provincia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_comuna`
--

LOCK TABLES `registro_comuna` WRITE;
/*!40000 ALTER TABLE `registro_comuna` DISABLE KEYS */;
INSERT INTO `registro_comuna` VALUES (3,'Calle Larga',5),(4,'Los Andes',5),(5,'Rinconada',5),(6,'San Esteban',5),(7,'Cabildo',6),(8,'La Ligua',6),(9,'Papudo',6),(10,'Petorca',6),(11,'Zapallar',6),(12,'Hijuelas',7),(13,'La Calera',7),(14,'La Cruz',7),(15,'Nogales',7),(16,'Quillota',7),(17,'Catemu',8),(18,'Llay-Llay',8),(19,'Panquehue',8),(20,'Putaendo',8),(21,'San Felipe',8),(22,'Santa María',8),(23,'Limache',9),(24,'Olmué',9),(25,'Quilpué',9),(26,'Villa Alemana',9),(27,'Colina',10),(28,'Lampa',10),(29,'Tiltil',10),(30,'Pirque',11),(31,'Puente Alto',11),(32,'San José de Maipo',11),(33,'Buin',12),(34,'Calera de Tango',12),(35,'Paine',12),(36,'San Bernardo',12),(37,'Alhué',13),(38,'Curacaví',13),(39,'María Pinto',13),(40,'Melipilla',13),(41,'San Pedro',13),(42,'El Monte',14),(43,'Isla de Maipo',14),(44,'Padre Hurtado',14),(45,'Peñaflor',14),(46,'Talagante',14),(47,'Codegua',15),(48,'Coinco',15),(49,'Coltauco',15),(50,'Doñihue',15),(51,'Graneros',15),(52,'Las Cabras',15),(53,'Machalí',15),(54,'Malloa',15),(55,'Mostazal',15),(56,'Olivar',15),(57,'Peumo',15),(58,'Pichidegua',15),(59,'Quinta de Tilcoco',15),(60,'Rancagua',15),(61,'Rengo',15),(62,'Requínoa',15),(63,'San Vicente de Tagua Tagua',15),(64,'La Estrella',16),(65,'Litueche',16),(66,'Marchihue',16),(67,'Navidad',16),(68,'Paredones',16),(69,'Pichilemu',16),(70,'Chépica',17),(71,'Chimbarongo',17),(72,'Lolol',17),(73,'Nancagua',17),(74,'Palmilla',17),(75,'Peralillo',17),(76,'Placilla',17),(77,'Pumanque',17),(78,'San Fernando',17),(79,'Santa Cruz',17),(80,'Curicó',18),(81,'Hualañé',18),(82,'Licantén',18),(83,'Molina',18),(84,'Rauco',18),(85,'Romeral',18),(86,'Sagrada Familia',18),(87,'Teno',18),(88,'Vichuquén',18),(89,'Colbún',19),(90,'Linares',19),(91,'Longaví',19),(92,'Parral',19),(93,'Retiro',19),(94,'San Javier',19),(95,'Villa Alegre',19),(96,'Yerbas Buenas',19),(97,'Constitución',20),(98,'Curepto',20),(99,'Empedrado',20),(100,'Maule',20),(101,'Pelarco',20),(102,'Pencahue',20),(103,'Río Claro',20),(104,'San Clemente',20),(105,'San Rafael',20),(106,'Talca',20);
/*!40000 ALTER TABLE `registro_comuna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_empresa`
--

DROP TABLE IF EXISTS `registro_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(200) NOT NULL,
  `rut` varchar(54) NOT NULL,
  `telefono` int(11) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `comuna_id` int(11) NOT NULL,
  `provincia_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `email_vendedor` varchar(200) DEFAULT NULL,
  `es_vendedor` tinyint(1) NOT NULL,
  `foto_empresa` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_empres_comuna_id_4b91bb62f574ae20_fk_registro_comuna_id` (`comuna_id`),
  KEY `registro_empresa_54bf7e76` (`provincia_id`),
  KEY `registro_empresa_0f442f96` (`region_id`),
  CONSTRAINT `registro__provincia_id_3bcbb6004be7d691_fk_registro_provincia_id` FOREIGN KEY (`provincia_id`) REFERENCES `registro_provincia` (`id`),
  CONSTRAINT `registro_empres_comuna_id_4b91bb62f574ae20_fk_registro_comuna_id` FOREIGN KEY (`comuna_id`) REFERENCES `registro_comuna` (`id`),
  CONSTRAINT `registro_empres_region_id_664300490caf7ebb_fk_registro_region_id` FOREIGN KEY (`region_id`) REFERENCES `registro_region` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_empresa`
--

LOCK TABLES `registro_empresa` WRITE;
/*!40000 ALTER TABLE `registro_empresa` DISABLE KEYS */;
INSERT INTO `registro_empresa` VALUES (1,'grungero','111111111',12345678,'direccion en alguna parte, providencia',30,11,14,'grungero@gmail.com',1,''),(3,'Koagra','13723050',7777777,'las torres 123',33,12,14,'buscagrochile@gmail.com',1,'fotos_empresas/bloggif_56a55b1d669f4.jpeg'),(4,'KALS','98765433',5555555,'Hualuz 2100',46,14,14,'buscagrochile@gmail.com',1,'/home/augustoa/webapps/buscagrostatic/media/fotos_empresas/empresa_xCpZWmp.jpg'),(5,'Agrozentro','87888888',99999999,'Longuitudinal 109',33,12,14,'buscagrochile@gmail.com',1,'fotos_empresas/bloggif_56a556e010ac2.jpeg'),(8,'Solcom SpA','764355261',66740772,'Av. Plaza 540, of. 13, Las Condes, Santiago, Chile',25,9,6,NULL,0,''),(9,'Agr. Padre Hurtado','12345678',23700319,'Camino Melipilla 445',44,14,14,'contacto@buscagro.cl',1,'/home/augustoa/webapps/buscagrostatic/media/fotos_empresas/dddd_VCZo0Bm.jpg'),(12,'Invesiones agrícolas SpA.','154723056',97921456,'las torres 300',33,12,14,NULL,0,''),(13,'asdasda','222222222',1231232131,'asdsadasdasda',4,5,6,'',0,'/home/augustoa/webapps/buscagrostatic/media/fotos_empresas/dddd_P284hGV.jpg');
/*!40000 ALTER TABLE `registro_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_especie`
--

DROP TABLE IF EXISTS `registro_especie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_especie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `tipo_especie_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_especie_321e2f0a` (`tipo_especie_id`),
  CONSTRAINT `registro_espe_tipo_especie_id_94525e1_fk_registro_tipoespecie_id` FOREIGN KEY (`tipo_especie_id`) REFERENCES `registro_tipoespecie` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_especie`
--

LOCK TABLES `registro_especie` WRITE;
/*!40000 ALTER TABLE `registro_especie` DISABLE KEYS */;
INSERT INTO `registro_especie` VALUES (1,'Carozos',1),(2,'Frutos Secos',1),(3,'Pomaceas',1),(4,'Berries',1),(5,'Cítricos',1),(6,'Uva de Mesa',1),(7,'Uva Vinífera',1),(8,'Maiz',2),(9,'Forrajeras',2),(12,'Hortalizas',2),(13,'Cereales',2);
/*!40000 ALTER TABLE `registro_especie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_producto`
--

DROP TABLE IF EXISTS `registro_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `archivo_pdf` varchar(100) DEFAULT NULL,
  `tipo_producto_id` int(11) NOT NULL,
  `foto_producto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_producto_ab4462f4` (`tipo_producto_id`),
  CONSTRAINT `r_tipo_producto_id_3f7eef5b877c3f93_fk_registro_tipoproductos_id` FOREIGN KEY (`tipo_producto_id`) REFERENCES `registro_tipoproductos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_producto`
--

LOCK TABLES `registro_producto` WRITE;
/*!40000 ALTER TABLE `registro_producto` DISABLE KEYS */;
INSERT INTO `registro_producto` VALUES (1,'producto test 1','',1,NULL),(2,'producto test 2','',1,NULL),(3,'Roundap','pdf/Roundup_03-07-2012.pdf',2,'fotos_productos/roundap.jpg'),(4,'Karate Zeon','pdf/KarateZeon.pdf',3,'fotos_productos/karate-zeon-1-l.jpg'),(5,'Urea granulada','pdf/urea.pdf',1,'fotos_productos/saeq.jpg'),(6,'Tractor 6125D John Deere','pdf/Tractor_John_Deere.pdf',11,'fotos_productos/jd_tractor.jpg'),(7,'Fast Plus','pdf/Etiqueta-Fast-Plus-EW.pdf',5,NULL),(8,'Bravo 720','pdf/Bravo720.pdf',4,'fotos_productos/be1120_bravo720.jpg'),(9,'Semilla Maiz P1780','pdf/P1780.pdf',6,'fotos_productos/semilla.jpg'),(10,'Sulfato de Potasio','pdf/FT_QUIMIFER_SULFATO_DE_POTASIO.pdf',1,'fotos_productos/sulftao_de_potasio.jpg'),(11,'Sulfato de Magnesio','pdf/SULFATO_DE_MAGNESIO.pdf',1,'fotos_productos/sulfato_Mg.jpg'),(12,'Dursban 48','pdf/ft-dursban-48.pdf',3,'fotos_productos/bloggif_56a937a7161a7.jpeg'),(13,'Rooting','pdf/Ficha_tecnica_del_Rooting.pdf',9,'fotos_productos/rooting.jpg'),(14,'prueba','',9,'fotos_productos/franciscaurria_114_974.jpg');
/*!40000 ALTER TABLE `registro_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_productor`
--

DROP TABLE IF EXISTS `registro_productor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_productor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_prod_empresa_id_5a08656e88040b7a_fk_registro_empresa_id` (`empresa_id`),
  CONSTRAINT `registro_prod_empresa_id_5a08656e88040b7a_fk_registro_empresa_id` FOREIGN KEY (`empresa_id`) REFERENCES `registro_empresa` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_productor`
--

LOCK TABLES `registro_productor` WRITE;
/*!40000 ALTER TABLE `registro_productor` DISABLE KEYS */;
INSERT INTO `registro_productor` VALUES (1,'grungero','grungero','grungero@gmail.com',1),(6,'Manuel Ossa','manolo1','mjossa@gmail.com',8),(9,'alexis','alexis','salvoalexis@yahoo.es',12),(10,'asdasdasd','123456','asdas@asd.cl',13);
/*!40000 ALTER TABLE `registro_productor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_productorespecie`
--

DROP TABLE IF EXISTS `registro_productorespecie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_productorespecie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hectarea` int(11) NOT NULL,
  `productor_id` int(11) NOT NULL,
  `tipo_especie_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro__productor_id_23e134b2593de7aa_fk_registro_productor_id` (`productor_id`),
  KEY `registro_productorespecie_321e2f0a` (`tipo_especie_id`),
  CONSTRAINT `regi_tipo_especie_id_7538de334f83ace8_fk_registro_tipoespecie_id` FOREIGN KEY (`tipo_especie_id`) REFERENCES `registro_tipoespecie` (`id`),
  CONSTRAINT `registro__productor_id_23e134b2593de7aa_fk_registro_productor_id` FOREIGN KEY (`productor_id`) REFERENCES `registro_productor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_productorespecie`
--

LOCK TABLES `registro_productorespecie` WRITE;
/*!40000 ALTER TABLE `registro_productorespecie` DISABLE KEYS */;
INSERT INTO `registro_productorespecie` VALUES (1,12,1,1),(2,12,1,1),(3,14,1,2),(12,100,6,2),(16,20,9,1);
/*!40000 ALTER TABLE `registro_productorespecie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_productorespecie_especie`
--

DROP TABLE IF EXISTS `registro_productorespecie_especie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_productorespecie_especie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productorespecie_id` int(11) NOT NULL,
  `especie_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `productorespecie_id` (`productorespecie_id`,`especie_id`),
  KEY `registro_prod_especie_id_5bdbcc739bd868e3_fk_registro_especie_id` (`especie_id`),
  CONSTRAINT `D0356b9bab97e0c70f448dea92288c3e` FOREIGN KEY (`productorespecie_id`) REFERENCES `registro_productorespecie` (`id`),
  CONSTRAINT `registro_prod_especie_id_5bdbcc739bd868e3_fk_registro_especie_id` FOREIGN KEY (`especie_id`) REFERENCES `registro_especie` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_productorespecie_especie`
--

LOCK TABLES `registro_productorespecie_especie` WRITE;
/*!40000 ALTER TABLE `registro_productorespecie_especie` DISABLE KEYS */;
INSERT INTO `registro_productorespecie_especie` VALUES (1,1,1),(2,2,3),(3,3,8),(12,12,8),(16,16,1);
/*!40000 ALTER TABLE `registro_productorespecie_especie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_provincia`
--

DROP TABLE IF EXISTS `registro_provincia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_provincia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `region_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_provincia_0f442f96` (`region_id`),
  CONSTRAINT `registro_provincia_region_id_2d61c199_fk_registro_region_id` FOREIGN KEY (`region_id`) REFERENCES `registro_region` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_provincia`
--

LOCK TABLES `registro_provincia` WRITE;
/*!40000 ALTER TABLE `registro_provincia` DISABLE KEYS */;
INSERT INTO `registro_provincia` VALUES (5,'Los Andes',6),(6,'Petorca',6),(7,'Quillota',6),(8,'San Felipe',6),(9,'Marga Marga',6),(10,'Chacabuco',14),(11,'Cordillera',14),(12,'Maipo',14),(13,'Melipilla',14),(14,'Talagante',14),(15,'Cachapoal',7),(16,'Cardenal Caro',7),(17,'Colchagua',7),(18,'Curicó',8),(19,'Linares',8),(20,'Talca',8);
/*!40000 ALTER TABLE `registro_provincia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_region`
--

DROP TABLE IF EXISTS `registro_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_region`
--

LOCK TABLES `registro_region` WRITE;
/*!40000 ALTER TABLE `registro_region` DISABLE KEYS */;
INSERT INTO `registro_region` VALUES (2,1,'Tarapacá'),(3,2,'Antofagasta'),(4,3,'Atacama'),(5,4,'Coquimbo'),(6,5,'Valparaiso'),(7,6,'O\'Higgins'),(8,7,'Maule'),(9,8,'BioBío'),(10,9,'La Araucanía'),(11,10,'Los Lagos'),(12,11,'Aysen'),(13,12,'Magallanes'),(14,13,'Región Metropolitana'),(15,14,'Los Ríos'),(16,15,'Arica y Parinacota');
/*!40000 ALTER TABLE `registro_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_sucursal`
--

DROP TABLE IF EXISTS `registro_sucursal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(700) DEFAULT NULL,
  `direccion` varchar(700) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  `latitud` varchar(255) DEFAULT NULL,
  `longitud` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registro_sucu_empresa_id_2e621e32b9dff873_fk_registro_empresa_id` (`empresa_id`),
  CONSTRAINT `registro_sucu_empresa_id_2e621e32b9dff873_fk_registro_empresa_id` FOREIGN KEY (`empresa_id`) REFERENCES `registro_empresa` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_sucursal`
--

LOCK TABLES `registro_sucursal` WRITE;
/*!40000 ALTER TABLE `registro_sucursal` DISABLE KEYS */;
INSERT INTO `registro_sucursal` VALUES (1,'Agrozentro','Av. Longuitudinal 190',25601390,5,'-33.73413','-70.73383'),(2,'Koagra','Los conquistadores 2800',23410951,3,'-33.77372','-70.74426'),(3,'KALS','Las rosas2111',99127888,4,'-33.659','-70.91587'),(4,' Agr. Padre Hurtado','Camino Melipilla 445',29900543,9,'-33.55107','-70.79854');
/*!40000 ALTER TABLE `registro_sucursal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_tipoespecie`
--

DROP TABLE IF EXISTS `registro_tipoespecie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_tipoespecie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_tipoespecie`
--

LOCK TABLES `registro_tipoespecie` WRITE;
/*!40000 ALTER TABLE `registro_tipoespecie` DISABLE KEYS */;
INSERT INTO `registro_tipoespecie` VALUES (1,1,'Frutales'),(2,2,'Semillas');
/*!40000 ALTER TABLE `registro_tipoespecie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_tipoproductos`
--

DROP TABLE IF EXISTS `registro_tipoproductos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_tipoproductos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_tipoproductos`
--

LOCK TABLES `registro_tipoproductos` WRITE;
/*!40000 ALTER TABLE `registro_tipoproductos` DISABLE KEYS */;
INSERT INTO `registro_tipoproductos` VALUES (1,1,'Fertilizantes'),(2,2,'Herbicida'),(3,3,'Insecticida'),(4,4,'Fungicida'),(5,5,'Acaracida'),(6,6,'Semilla'),(7,7,'riego'),(8,8,'Vivero'),(9,9,'Fitorregulador'),(10,10,'Laboratorio'),(11,11,'Maquinaria'),(12,12,'Herramienta');
/*!40000 ALTER TABLE `registro_tipoproductos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_tips`
--

DROP TABLE IF EXISTS `registro_tips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_tips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(700) DEFAULT NULL,
  `direccion` varchar(700) DEFAULT NULL,
  `telefono` varchar(700) DEFAULT NULL,
  `latitud` varchar(255) DEFAULT NULL,
  `longitud` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_tips`
--

LOCK TABLES `registro_tips` WRITE;
/*!40000 ALTER TABLE `registro_tips` DISABLE KEYS */;
INSERT INTO `registro_tips` VALUES (1,'Datos Agroclimáticos Huelquen (13/04)','GDA: 1.658; T°Max: 19,4°; T°Min: 5,6°',NULL,'-33.862083','-70.639194'),(2,'Datos Agroclimáticos Talagante (13/04)','GDA: 1.646; T°Max: 17°; T°Min: 7,5°',NULL,'-33.67325','-70.921139');
/*!40000 ALTER TABLE `registro_tips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_usuario`
--

DROP TABLE IF EXISTS `registro_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_usuario`
--

LOCK TABLES `registro_usuario` WRITE;
/*!40000 ALTER TABLE `registro_usuario` DISABLE KEYS */;
INSERT INTO `registro_usuario` VALUES (1,'buscagrochile1@gmail.com'),(2,'alexis.salvo@buscagro.cl');
/*!40000 ALTER TABLE `registro_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-22 21:17:18
