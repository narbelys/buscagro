from django.db import models

# Create your models here.
class Usuario(models.Model):
	email = models.CharField(max_length=200)

	def __unicode__(self):
		return self.email

class Region(models.Model):
	numero = models.IntegerField()
	nombre = models.CharField(max_length=200)

	def __unicode__(self):
		return self.nombre

class Provincia(models.Model):
	region = models.ForeignKey(Region)
	nombre = models.CharField(max_length=200)

	def __unicode__(self):
		return self.nombre - " - Region de " + self.empresa.nombre

class Comuna(models.Model):
	provincia = models.ForeignKey(Provincia)
	nombre = models.CharField(max_length=200)

	def __unicode__(self):
		return self.nombre - " - Provincia de " + self.provincia.nombre

class Empresa(models.Model):
	razon_social = models.CharField(max_length=200)
	rut = models.IntegerField()
	telefono = models.IntegerField()
	direccion = models.CharField(max_length=200)
	region = models.ForeignKey(Region)
	provincia = models.ForeignKey(Provincia)
	comuna = models.ForeignKey(Comuna)

	def __unicode__(self):
		return self.username

class Productor(models.Model):
	username = models.CharField(max_length=200)
	password = models.CharField(max_length=200)
	email = models.CharField(max_length=200)

	def __unicode__(self):
		return self.username

class TipoEspecie(models.Model):
	codigo = models.IntegerField()
	nombre = models.CharField(max_length=200)

	def __unicode__(self):
		return str(self.codigo) + " - " + self.nombre

class Especie(models.Model):
	nombre = models.CharField(max_length=200)
	tipo_especie = models.ForeignKey(TipoEspecie)

	def __unicode__(self):
		return self.nombre

class ProductorEspecie(models.Model):
	productor = models.ForeignKey(Productor)
	especie = models.ForeignKey(Especie)
	hectarea = models.IntegerField()

	def __unicode__(self):
		return self.productor.username + " - Especie: " + self.especie.nombre + " - "+ str(self.hectarea)+" Hecareas"

class Producto(models.Model):
 	nombre = models.CharField(max_length=200)