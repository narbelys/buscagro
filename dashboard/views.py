# Create your views here.
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


from dashboard.models import *

def home(request):
	data = {'mostrar':'no',
			'existe':'no'}
	c = RequestContext(request, data)
	return render_to_response('index.html',c)

@csrf_exempt
def guardar_email(request):
	email = request.POST['email']
	mostrar = 'si'
	existe = 'no'
	try:
		Usuario.objects.get(email=email)
		mostrar = 'no'
		existe = 'si'
	except Usuario.DoesNotExist:
		Usuario.objects.create(email=email)
		

	data = {'mostrar':mostrar,
			'existe':existe}
	c = RequestContext(request, data)
	return render_to_response('index.html',c)

def buscar(request):
	data = {'mostrar':'no',
			'existe':'no'}
	c = RequestContext(request, data)
	return render_to_response('interior.html',c)

def quienes_somos(request):
	data = {'mostrar':'no',
			'existe':'no'}
	c = RequestContext(request, data)
	return render_to_response('quienes-somos.html',c)

def preguntas_frecuentes(request):
	data = {'mostrar':'no',
			'existe':'no'}
	c = RequestContext(request, data)
	return render_to_response('preguntas-frecuentes.html',c)

def politicas(request):
	data = {'mostrar':'no',
			'existe':'no'}
	c = RequestContext(request, data)
	return render_to_response('politicas.html',c)

def terminos_condiciones(request):
	data = {'mostrar':'no',
			'existe':'no'}
	c = RequestContext(request, data)
	return render_to_response('terminos-y-condiciones.html',c)